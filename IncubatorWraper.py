
if __name__ == '__main__':
	import Incubator as Environment
	import sys, os, gc, ConfigParser, timeit
	sys.path.insert(0,os.getcwd())
	import Code.Data_Writer.LogKeeper as LogKeeper
	import AspectFlags
	from time import clock # clock more acurate on windows replace with time for unix

	# This InubatorWrapper starts the process based on two configuration files, the name of which are passed as arguments to the porgram
	# The first argument (config file) is the switches config file, this denotes all of the properties of the run.
	# The second argument is the paths config file, this contains the directories for test files to be run as well as the program to patch
		# For layout of the of these configuration files please see sampleSwitches.cfg and smaplePaths.cfg
	# The third argument is the trial number. This argument it used for when running large numbers of trials using scripts.
		#NOTE: must be >= 1

	LogKeeper.MasterLog = LogKeeper.LogKeeper(sys.argv[2]+'-'+sys.argv[3],'')
	
	reader = open(sys.argv[1], 'r')
	LogKeeper.MasterLog.write_Log(reader.read())
	LogKeeper.MasterLog.write_Log('\n\n')

	reader = open(sys.argv[2], 'r')
	LogKeeper.MasterLog.write_Log(reader.read())
	LogKeeper.MasterLog.write_Log('\n\n')

	configSwitches = ConfigParser.ConfigParser()
	configSwitches.read(sys.argv[1])

	configPaths = ConfigParser.ConfigParser()
	configPaths.read(sys.argv[2])

	AspectFlags.tarantula = configSwitches.getboolean('AspectFlags','tarantula')
	AspectFlags.xOver = configSwitches.getboolean('AspectFlags','xOver')
	AspectFlags.mutation = configSwitches.getboolean('AspectFlags','mutation')
	
	AspectFlags.varSwaps = configSwitches.getboolean('AspectFlags','varSwaps')
	AspectFlags.opSwaps = configSwitches.getboolean('AspectFlags','opSwaps')
	AspectFlags.varWeights = configSwitches.getboolean('AspectFlags','varWeights')
	AspectFlags.opWeights = configSwitches.getboolean('AspectFlags','opWeights')
	AspectFlags.minEditWeight = configSwitches.getboolean('AspectFlags','minEditWeight')
	
	AspectFlags.timeout = configSwitches.getfloat('AspectFlags','timeout')
	AspectFlags.maxNumChanges = configSwitches.getint('AspectFlags','maxNumChanges')
	
	AspectFlags.contAfterSol = configSwitches.getboolean('AspectFlags','contAfterSol')

	LogKeeper.tarantulaWeights = configSwitches.getboolean('LogKeepingFlags','tarantulaWeights')
	LogKeeper.patch = configSwitches.getboolean('LogKeepingFlags','patch')
	LogKeeper.patchOperations = configSwitches.getboolean('LogKeepingFlags','patchOperations')
	LogKeeper.fitnessTest = configSwitches.getboolean('LogKeepingFlags','fitnessTest')
	LogKeeper.fitnessValue = configSwitches.getboolean('LogKeepingFlags','fitnessValue')
	LogKeeper.individuals = configSwitches.getboolean('LogKeepingFlags','individuals')
	LogKeeper.printIndividuals = configSwitches.getboolean('LogKeepingFlags','printIndividuals')
	LogKeeper.printAppliedIndividuals = configSwitches.getboolean('LogKeepingFlags','printAppliedIndividuals')
	LogKeeper.OffspringStats = configSwitches.getboolean('LogKeepingFlags','OffspringStats')
	LogKeeper.opWeights = configSwitches.getboolean('LogKeepingFlags','opWeights')
	LogKeeper.varWeights = configSwitches.getboolean('LogKeepingFlags','varWeights')

	fullStart = clock()
		
	LogKeeper.MasterLog.write_Log('\nStart run: '+sys.argv[1]+'\n')
	start = clock()
	#import cProfile
	#cProfile.run('Environment.StartIncubator(configSwitches, configPaths)')
	Environment.StartIncubator(configSwitches, configPaths)
	end = clock()
	LogKeeper.MasterLog.write_Log('\ntrial '+sys.argv[3]+'of'+sys.argv[2]+' complete in '+str(end - start)+' seconds\n')
	fullEnd = clock()
	LogKeeper.MasterLog.write_Log("Completed task: run "+sys.argv[3]+" of "+sys.argv[2]+" in "+str(fullEnd-fullStart)+" seconds")
