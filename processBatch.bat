
@echo off
setLocal EnableDelayedExpansion

for /f "tokens=* delims= " %%a in (configList.txt) do (
	set /a N+=1
	for /L %%p in (1,1,2) do (
		IncubatorWraper.py %1 %%a %%p
	)
)