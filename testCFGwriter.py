import ConfigParser

config = ConfigParser.ConfigParser()
config.read('example.cfg')

config.add_section('TrialInfo')
config.set('TrialInfo', 'trialNumber', '0')
config.set('TrialInfo', 'trialSet', 'SummerTrials')
config.set('TrialInfo', 'maxGenerations', '50')
config.set('TrialInfo', 'populationSize', '50')
config.set('TrialInfo', 'mutation', 'True')
config.set('TrialInfo', 'mutationRate', '0.1')
config.set('TrialInfo', 'xover', 'True')
config.set('TrialInfo', 'xoverRate', '0.3')

config.add_section('Paths')
config.set('Paths', 'modulePath', 'Code.Artifical_Benchmarks.Summer.')
config.set('Paths', 'filePath', '\\Code\\Artifical_Benchmarks\\Summer\\')

config.add_section('SourceFiles')
config.set('SourceFiles', 'sourceFile1', 'summer')
config.set('SourceFiles', 'sourceFile2', 'summerpt2')

config.add_section('TestFiles')
config.set('TestFiles', 'testFile1', 'TestSummer')


# Writing our configuration file to 'Incubator.cfg'
with open('Incubator.cfg', 'w') as configfile:
    config.write(configfile)
