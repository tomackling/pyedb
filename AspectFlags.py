# Aspect Flags
# Values here are default values.

tarantula = True
xOver = True
mutation = True

varSwaps = True
varWeights = True
opSwaps = True
opWeights = True
minEditWeight = True

contAfterSol = False

timeout = 4.0
numOfTests = 100
maxNumChanges = 8
