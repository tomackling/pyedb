

if __name__ == '__main__':
	# BitStringEvaluator Test

	from Code.Bugfix.evaluators.BitStringEvaluator import *
	from Code.Bugfix.Individual import Individual
	from Code.PatcherUtilities.PreProcessor import PreProcessor
	from Code.PatcherUtilities.Patcher import Patcher

	modulePath = 'Code.Artifical_Benchmarks.Summer.'
	filePath = 'C:\\Users\\Thomas\\Desktop\\Patch Evolution New\\Code\\Artifical_Benchmarks\\Summer\\'
	sourceFiles = []
	sourceFiles.append('summer')
	sourceFiles.append('summerpt2')
	testFile = 'TestSummer'

	PP = PreProcessor(modulePath, filePath, sourceFiles, testFile)
	PP.preProcess()

	result = PP.getResults()	# Result is tuple of form: ASTList, numberOfNodes, treeWeights
	ASTList = result[0]
	numberOfNodes = result[1]
	treeWeights = result[2]

	patchApplier = Patcher(ASTList, numberOfNodes, treeWeights)

	testIndividual = Individual('000')

	evaluator = BitStringEvaluator(patchApplier, filePath, testFile)

	evaluator.evaluate(testIndividual, 0)