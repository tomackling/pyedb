
def StartIncubator(configSwitches, configPaths):
	import AspectFlags
	import sys, os, gc, random, ConfigParser
	sys.path.insert(0,os.getcwd())
	import Code.Data_Writer.LogKeeper as LogKeeper
	from Code.PatcherUtilities.PreProcessor import PreProcessor
	from Code.PatcherUtilities.Patcher import Patcher
	from Code.Bugfix.Breeder import Breeder
	from Code.Bugfix.Individual import Individual
	from Code.Bugfix.evaluators.BitStringEvaluator import BitStringEvaluator
	from Code.Bugfix.selectors.Tournament import Tournament
	from Code.Bugfix.operators.MutationOperator import MutationOperator 			# deletion / insertion of 8 bits
	from Code.Bugfix.operators.MutationOperatorVer2 import MutationOperatorVer2 	# replacement of 8 bits
	from Code.Bugfix.operators.UXOverOperator import UXOverOperator 				# uniform crossover
	from Code.Bugfix.operators.UXOverOperatorVer2 import UXOverOperatorVer2			# uniform crossover -> extends both to longest
	from Code.Bugfix.operators.CutNSpliceOperator import CutNSpliceOperator 		# cut and splice at 32 bit boundries
	from Code.Bugfix.operators.CutNSpliceOperatorVer2 import CutNSpliceOperatorVer2 # hack and slash -> no boundry alignment
	from Code.Bugfix.operators.CutNSpliceOperatorReversal import CutNSpliceOperatorReversal # no boundry alignment plus reversal of chunk order from genomes 50%
	
	#Retrieve information from the configParser object and begin evolution process

	modulePath = configPaths.get('Paths','modulePath')
	filePath = os.getcwd()+configPaths.get('Paths', 'filePath')
	sourceFiles = []
	for srcFileNum, srcFile in configPaths.items('SourceFiles'):
		sourceFiles.append(srcFile)
	testFile = configPaths.get('TestFiles', 'testFile1')

	PP = PreProcessor(modulePath, filePath, sourceFiles, testFile)
	PP.preProcess()

	result = PP.getResults()
	ASTList = result[0]
	opList = result[1]
	varList = result[2]

	patchApplier = Patcher(ASTList, opList, varList)

	breeder = Breeder()
	breeder.selector = Tournament()
	breeder.selector.tournamentSize = configSwitches.getint('TrialInfo', 'tournamentSize')

	breeder.evaluator = BitStringEvaluator(patchApplier, filePath, testFile)

	breeder.operators = []
	if configSwitches.getboolean('AspectFlags', 'mutation'):
		if configSwitches.getint('AspectFlags', 'mutationType') == 0:
			Op1 = MutationOperator()
			Op1.probability = configSwitches.getfloat('TrialInfo', 'mutationRate')
			breeder.operators.append(Op1)
		else:
			Op1 = MutationOperatorVer2()
			Op1.probability = configSwitches.getfloat('TrialInfo', 'mutationRate')
			breeder.operators.append(Op1)
	if configSwitches.getboolean('AspectFlags', 'xover'):
		XOverType = configSwitches.getint('AspectFlags', 'xoverType')
		if XOverType == 0:
			Op2 = UXOverOperator()
			Op2.probability = configSwitches.getfloat('TrialInfo', 'xoverRate')
			breeder.operators.append(Op2)
		elif XOverType == 1:
			Op2 = CutNSpliceOperator()
			Op2.probability = configSwitches.getfloat('TrialInfo', 'xoverRate')
			breeder.operators.append(Op2)
		elif XOverType == 2:
			Op2 = CutNSpliceOperatorVer2()
			Op2.probability = configSwitches.getfloat('TrialInfo', 'xoverRate')
			breeder.operators.append(Op2)
		else:
			Op2 = CutNSpliceOperatorReversal()
			Op2.probability = configSwitches.getfloat('TrialInfo', 'xoverRate')
			breeder.operators.append(Op2)
	breeder.maxGenerations = configSwitches.getint('TrialInfo', 'maxGenerations')
	breeder.populationSize = configSwitches.getint('TrialInfo', 'populationSize')
	
	#opLength = 64
	opLength = configSwitches.getint('TrialInfo','initialGenotypeLength')
	maxNumOfOps = AspectFlags.maxNumChanges

	# data collection to sample search space
	#for op in range (2,3):
	#	for loc in range (0,64):
	#		for change in range(0,32):
	#			bitstring = bin(op)[2:].zfill(2) + '00000000' + bin(loc)[2:].zfill(7) + '00000000000' + bin(change)[2:].zfill(4)
	#			print(bitstring)
	#			geno = bitstring
	#			ind = Individual(geno)
	#			breeder.population.append(ind)	
	
	#proper initialisation
	for i in xrange(1,breeder.populationSize+1):
		geno = bin(random.getrandbits((1+((i%opLength)%maxNumOfOps))*opLength)).lstrip('-0b')
		while len(geno) < (1+((i%opLength)%maxNumOfOps))*opLength:
			if random.random() < 0.5:
				geno = geno+'0'
			else:
				geno = geno+'1'
		ind = Individual(geno)
		breeder.population.append(ind)
				
	breeder.breed()	
	gc.collect()



