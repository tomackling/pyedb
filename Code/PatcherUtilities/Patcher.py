
# When Patching is on must record, the genotype used to patch, and each individual operation applied by that genotype in a readable format

from ast import *
from OpExecutor.OpExecutor import *
import Code.Data_Writer.LogKeeper as LogKeeper

class Patcher(object):
	
	def __init__(self, OriginalAsts, opList, varList):
		self.BaseAsts = OriginalAsts
		self.varList = varList
		self.opList = opList
		self.executor = None
	
	def set_Up_Executor(self):
		self.executor = OpExecutor(self.BaseAsts, self.opList, self.varList)
	
	def patch_With_Genotype(self, genotype):
		if LogKeeper.patch:
			LogKeeper.MasterLog.write_Log('patching with '+genotype+'\n')
		length = len(genotype)
		offset = 0
		while length >= 32:
			self.executor.executeOp(genotype[offset:offset+32])
			offset += 32
			length -= 32

	def get_ASTList_For_Evaluation(self):
		return self.executor.getPatchedASTList()