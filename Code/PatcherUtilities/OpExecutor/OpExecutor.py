
import ast, copy, random, math, AspectFlags
from OpClasses import *


class OpExecutor(object):
	
	def __init__(self, ASTList, opLists, varLists):
		self.ASTList = copy.deepcopy(ASTList) #pickle?
		self.varLists = varLists
		self.opLists = opLists
		self.opCode = None
		self.opPrefix = None
		self.opDataA = None
		self.opDataB = None
		self.opCodeFunctions = dict()
		if AspectFlags.opSwaps:
			self.opCodeFunctions[0] = self.swap_Operator
		if AspectFlags.varSwaps:
			self.opCodeFunctions[1] = self.swap_Var_Name

	def executeOp(self, operation):
		self.opCode = operation
		self.opPrefix = int(self.opCode[0:2], 2)%len(self.opCodeFunctions)
		if AspectFlags.opSwaps:
			if AspectFlags.varSwaps:
				# If both opSwaps and varSwaps are ON
				if self.opPrefix == 0: # opSwaps
					self.opDataA = int(self.opCode[2:18], 2)
					self.opDataB = int(operation[18:32], 2)
					self.ASTList = self.opCodeFunctions[0]()
				elif self.opPrefix == 1: # varSwaps
					self.opDataA = int(operation[2:17], 2)
					self.opDataB = int(operation[17:32], 2)
					self.ASTList = self.opCodeFunctions[1]()
			else:
				# If only opSwaps are ON
				self.opDataA = int(self.opCode[2:18], 2)
				self.opDataB = int(operation[18:32], 2)
				self.ASTList = self.opCodeFunctions[0]()
		elif AspectFlags.varSwaps:
			# If only varSwaps are ON
			self.opDataA = int(operation[2:17], 2)
			self.opDataB = int(operation[17:32], 2)
			self.ASTList = self.opCodeFunctions[1]()
		else:
			print 'niether opSwaps or varSwaps are on one must be on please adjust acordingly'
		
	def specBinSearch(self,a,x,lo=0,hi=None):
		
		if hi is None:
			hi = len(a)-1
		if lo > hi:
			return -1
		mid = (lo + hi)//2
		if (x <= a[mid][1]) and (mid == 0):
			return mid
		if x <= a[mid][1] and x > a[mid-1][1]:
			return mid
		if x <= a[mid-1][1]:
			return self.specBinSearch(a,x,lo,mid-1)
		if x > a[mid][1]:
			return self.specBinSearch(a,x,mid+1,hi)
		print 'ERROR'
		
	def oldBinSearch(self,a, x, lo=0, hi=None):
		#for b in a:
			#print b
		mid = None
		if hi is None:
			hi = len(a)-1
		while lo < hi:
			mid = (lo+hi)//2
			midval = a[mid]
			#print lo, hi, mid, x
			#print a[mid]
			#print midval[1], x, midval[1]+midval[2]
			if midval[1] <= x and midval[1]+midval[2] >= x:
				return mid
			elif midval[1]+midval[2] <= x:
				lo = mid+1
			elif midval[1] >= x:
				hi = mid-1
		#print 'ret bad'
		return -1
	
	def getPatchedASTList(self):
		return self.ASTList	
	
	def swap_Var_Name(self):
	
		indexA = self.opDataA/(float(2**15))
		indexBaseB = self.opDataB/(float((2**15)))
		if AspectFlags.varWeights:
			varCount = self.specBinSearch(self.varLists[0],indexA)
			varDetail = self.varLists[0][varCount]
		else:
			varCount = self.opDataA%len(self.varLists[0])
			varDetail = self.varLists[0][varCount]
			varCount += 1
		if AspectFlags.minEditWeight:
			indexB = int(math.floor((indexBaseB**3)*(len(varDetail[4]))))
		else:
			indexB = self.opDataB%len(varDetail[4])
		#print varDetail
		varToSwap = varDetail[3]
		lineNum = varDetail[2]
		sourceName = varDetail[0]
		varForSwap = varDetail[4][indexB][0]
		#print varForSwap
		targets = self.varLists[1]
		TreeModifier = swap_Var_Name_NT(self.ASTList, varCount, varToSwap, varForSwap, lineNum, sourceName, targets)
		return TreeModifier.traverse_and_transform(self.opCode)
		
	def swap_Operator(self):
	
		indexA = self.opDataA/(float(2**16))
		indexB = self.opDataB
		if AspectFlags.opWeights:
			opCount = self.specBinSearch(self.opLists,indexA)
			opDetail = self.opLists[opCount]
		else:
			opCount = self.opDataA%len(self.opLists)
			opDetail = self.opLists[opCount]
		opToSwap = opDetail[3]
		opForSwap = opDetail[4][indexB%len(opDetail[4])]
		lineNum = opDetail[2]
		sourceName = opDetail[0]
		opCount += 1
		
		TreeModifier = swap_Operator_NT(self.ASTList, opCount, opToSwap, opForSwap, lineNum, sourceName)
		return TreeModifier.traverse_and_transform(self.opCode)
		
