

# Operation Classes for each transformation.

import ast, sys, os
sys.path.insert(0,os.getcwd())

import Code.Data_Writer.LogKeeper as LogKeeper
from CallBase import callbase
from GP_AST_Transformer import GP_AST_Transformer as TRANS


####	
#class swap_Eq_for_NotEq_NT(TRANS):
#	
#	def visit_Eq(self, curLine, node):
#		return ast.copy_location(ast.NotEq(),node)
#	
#class swap_NotEq_for_Eq_NT(TRANS):
#	
#	def visit_NotEq(self, curLine, node):
#		return ast.copy_location(ast.Eq(),node)
####

# Variable Swaper Transform Class

class swap_Var_Name_NT(TRANS):
	
	def __init__(self, ASTList, varCount, varToSwap, varForSwap, lineNum, sourceName, targets):
		self.ASTList = ASTList
		self.varToSwap = varToSwap
		self.varForSwap = varForSwap
		self.lineNum = lineNum
		self.sourceName = sourceName
		self.varCount = varCount
		self.varCounter = 0
		self.targets = targets
		
	def visit_Name(self, curLine, node):
		if (node.id,curLine) in self.targets:
			#print node.id, curLine, self.varCounter
			self.varCounter += 1
		#if node.id == 'distnace' and self.varToSwap == 'distnace':
		#print self.varCounter, self.varCount
		#print node.id,'->',self.varForSwap
		if self.varCounter == self.varCount:
			#print self.curFile, self.sourceName
			if self.curFile == self.sourceName:
				#print curLine, self.lineNum
				if curLine == self.lineNum:
		#			print node.id, self.varForSwap
					if LogKeeper.patchOperations:
						LogKeeper.MasterLog.write_Log('At line: '+str(curLine)+' Swapping: '+self.varToSwap+' For: '+self.varForSwap+'\n')
					newNode = ast.Name(name=self.varForSwap,ctx=node.ctx)
					#print self.varForSwap
					newNode.id = self.varForSwap
					return ast.copy_location(newNode,node)
		return node
		
# Operator Swaper Transform Class

class swap_Operator_NT(TRANS):

	def __init__(self, ASTList, opCount, opToSwap, opForSwap, lineNum, sourceName):
		self.ASTList = ASTList
		self.opCount = opCount
		self.opToSwap = opToSwap
		self.opForSwap = opForSwap
		self.lineNum = lineNum
		self.sourceName = sourceName
		self.opCounter = 0
		
	def replacer(self, node):
		if self.opForSwap == 'LtE':
			return ast.copy_location(ast.LtE(),node)
		elif self.opForSwap == 'GtE':
			return ast.copy_location(ast.GtE(),node)
		elif self.opForSwap == 'Lt':
			return ast.copy_location(ast.Lt(),node)
		elif self.opForSwap == 'Gt':
			return ast.copy_location(ast.Gt(),node)
		if LogKeeper.patchOperations: # IF IT GOT HERE REPORT FAILURE
			LogKeeper.MasterLog.write_Log('NOT COMPLETED\n')
		return node
	
	def visit_LtE(self, curLine, node):
		#print 'At line: '+str(curLine)+' Swapping: '+self.opToSwap+' For: '+self.opForSwap+'\n'
		self.opCounter += 1
		if self.opCounter == self.opCount:
			if curLine == self.lineNum:
				#if node.__class__.__name__ == self.opToSwap:
				if LogKeeper.patchOperations:
					LogKeeper.MasterLog.write_Log('At line: '+str(curLine)+' Swapping: '+self.opToSwap+' For: '+self.opForSwap+'\n')
				return self.replacer(node)
		return node
		
	def visit_GtE(self, curLine, node):
		#print 'At line: '+str(curLine)+' Swapping: '+self.opToSwap+' For: '+self.opForSwap+'\n'
		self.opCounter += 1
		if self.opCounter == self.opCount:
			if curLine == self.lineNum:
				#if node.__class__.__name__ == self.opToSwap:
				if LogKeeper.patchOperations:
					LogKeeper.MasterLog.write_Log('At line: '+str(curLine)+' Swapping: '+self.opToSwap+' For: '+self.opForSwap+'\n')
				return self.replacer(node)
		return node
		
	def visit_Lt(self, curLine, node):
		#print 'At line: '+str(curLine)+' Swapping: '+self.opToSwap+' For: '+self.opForSwap+'\n'
		self.opCounter += 1
		if self.opCounter == self.opCount:
			if curLine == self.lineNum:
				#if node.__class__.__name__ == self.opToSwap:
				if LogKeeper.patchOperations:
					LogKeeper.MasterLog.write_Log('At line: '+str(curLine)+' Swapping: '+self.opToSwap+' For: '+self.opForSwap+'\n')
				return self.replacer(node)
		return node
		
	def visit_Gt(self, curLine, node):
		#print 'At line: '+str(curLine)+' Swapping: '+self.opToSwap+' For: '+self.opForSwap+'\n'
		self.opCounter += 1
		#print self.opCounter, self.opCount
		if self.opCounter == self.opCount:
			#print curLine, self.lineNum
			if curLine == self.lineNum:
				#print node.__class__.__name__, self.opToSwap
				#if node.__class__.__name__ == self.opToSwap:
				if LogKeeper.patchOperations:
					LogKeeper.MasterLog.write_Log('At line: '+str(curLine)+' Swapping: '+self.opToSwap+' For: '+self.opForSwap+'\n')
				return self.replacer(node)
		return node
		
