
import sys, os
sys.path.insert(0,os.getcwd()) 

from ast import *
import Code.Data_Writer.LogKeeper as LogKeeper

class GP_AST_Transformer(NodeTransformer):
	
	def __init__(self, ASTList):
		pass
		
	def traverse_and_transform(self, opCode):
		""" Traverse the ASTs altering valid nodes. """
		self.curAddress = 0

		#
		if LogKeeper.patchOperations:
			LogKeeper.MasterLog.write_Log('Applied operation: '+self.__class__.__name__+' '+str(opCode)+' Changes: '+self.sourceName+'\n')
		##
		
		for source in self.ASTList:
			self.curFile = source
			self.ASTList[self.curFile]
			self.visit(0,self.ASTList[self.curFile])
			
		return self.ASTList

	def visit(self, curLine, node):

		# Incrementing address
		hasLineNum = getattr(node, 'lineno', None)
		if hasLineNum is not None:
			curLine = node.lineno
			#key = self.curFile, node.lineno
			#if key in self.weightsTable:
			#	self.curAddress += self.weightsTable[key]
		#else:
			#key = self.curFile, curLine
			#if key in self.weightsTable:
			#	self.curAddress += self.weightsTable[key]
		# Determining which visitor method should be used.
		#if (self.curAddress >= self.address) and (self.curAddress <= self.address+self.extent):
		method = 'visit_' + node.__class__.__name__
		visitor = getattr(self, method, self.generic_visit)
		#else:
		#	visitor = self.generic_visit
		return visitor(curLine,node)
		
	def generic_visit(self, curLine, node):
		for field, old_value in iter_fields(node):
			old_value = getattr(node, field, None)
			if isinstance(old_value, list):
				new_values = []
				for value in old_value:
					if isinstance(value, AST):
						value = self.visit(curLine,value)
						if value is None:
							continue
						elif not isinstance(value, AST):
							new_values.extend(value)
							continue
					new_values.append(value)
				old_value[:] = new_values
			elif isinstance(old_value, AST):
				new_node = self.visit(curLine,old_value)
				if new_node is None:
					delattr(node, field)
				else:
					setattr(node, field, new_node)
		return node

