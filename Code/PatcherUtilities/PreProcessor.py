
import sys, os, copy
sys.path.insert(0,os.getcwd())
import Code.Data_Writer.LogKeeper as LogKeeper
import AspectFlags
from ast import *
from Code.pyflakes.checker import Checker
from multiprocessing import Process, Queue
from TreePreProcessor import *
from TarantulaClone import *
from TestProcesses import TaranTestProcess
import Queue as QUE

class PreProcessor(object):

	def __init__(self, modulePath, filePath, sourceFiles, testFile):
		
		# Constat Fields
		self.modulePath = modulePath
		self.filePath = filePath
		self.sourceFiles = sourceFiles
		self.testFile = testFile
		self.TPP = TreePreProcessor()
		self.spiderNest = TarantulaClone()
		
		# Result Fields
		self.ASTList = None
		self.numberOfNodes = None
		self.treeWeights = None
		self.opList = None
		self.varList = None
	
	def preProcess(self):

		# Get sourcefiles and compile into asts
		self.ASTList = dict()
		for source in self.sourceFiles:
			myAst = compile(open(self.filePath+source+'.py').read(), self.filePath+source+'.py', 'exec', PyCF_ONLY_AST)
			self.ASTList[source] = myAst 

		# Get the total number of nodes in the source trees and the node count per line
		self.numberOfNodes = 0
		nodesPerLine = dict()
		for key in self.ASTList:
			self.numberOfNodes += self.TPP.countNodes(self.ASTList[key])
			nodesPerLine = self.TPP.lineNodeCount(key,self.ASTList[key])
		
		self.treeWeights = dict()
		
		taranSuccess = False
		if AspectFlags.tarantula:
			# Get relative bug probabilities from tarantula
			
			q = Queue(1)
			
			args = dict()
			args['spiderNest'] = self.spiderNest
			args['modulePath'] = self.modulePath
			args['filePath'] = self.filePath
			args['testFile'] = self.testFile
			args['sourceFiles'] = self.sourceFiles
			
			q.put(args)
			
			tester = Process(target = TaranTestProcess, args = (q,))
			tester.daemon = True
			tester.start()
			tester.join(AspectFlags.timeout)
			
			try:
				result = q.get_nowait()
				taranSuccess = True
			except QUE.Empty:
				#
				if LogKeeper.tarantulaWeights:
					LogKeeper.MasterLog.write_Log('Unable to gather weighting data \n Will attempt to evolve with default weights:\n')
				##
				if tester.is_alive():
					tester.terminate()
				taranSuccess = False
			
			if taranSuccess:
				
				table = result[0]
				taranTime = result[1]
				numOfTests = result[2]
				AspectFlags.timeout = taranTime
				AspectFlags.numOfTests = numOfTests
				
				#
				if LogKeeper.tarantulaWeights:
					LogKeeper.MasterLog.write_Log('Tarantula results:\n')
				##
				
				# use data gathered to calculate weighting for each node on a particular line.
				for key in table:
					if key in nodesPerLine:
						self.treeWeights[key] = (table[key]/nodesPerLine[key])*self.numberOfNodes
						
						#
						if LogKeeper.tarantulaWeights:
							LogKeeper.MasterLog.write_Log(str(key)+','+str(table[key])+','+str(self.treeWeights[key])+'\n')
						##
				#
				LogKeeper.MasterLog.write_Log('\n')
				##
			
		if not taranSuccess:
			for key in nodesPerLine:
				self.treeWeights[key] = 1
				
				#
				if LogKeeper.tarantulaWeights:
					LogKeeper.MasterLog.write_Log(str(key)+','+str(self.treeWeights[key])+'\n')
				##
				
			#
			LogKeeper.MasterLog.write_Log('\n')
			##
		
		self.varList = []
		self.opList = []
		if AspectFlags.varSwaps or AspectFlags.opSwaps:
			for source in self.ASTList:
				
				chkr = Checker(self.ASTList[source])
				
				if AspectFlags.varSwaps:
					keyItem = chkr.scopeVars
					keyItem.sort(key=lambda var: var[0][0])
					#print keyItem
					cumWeightVar = 0
					for element in keyItem:
						lineNum = element[0][0]
						varToSwap = element[0][1]
						varsForSwap = element[1]
						if (varToSwap,0) in varsForSwap:
							varsForSwap.remove((varToSwap,0))
						if (source,lineNum) in self.treeWeights:
							rangeVar = self.treeWeights[(source,lineNum)]
							if rangeVar == 0:
								rangeVar = 0.005
						else:
							rangeVar = 0.005
						cumWeightVar += rangeVar
						self.varList.append([source,cumWeightVar,lineNum,varToSwap,varsForSwap])

						
				if AspectFlags.opSwaps:
					opNodes = chkr.operatorNodes
					opNodes = sorted(opNodes, key=lambda opInfo: opInfo[0])
					opSwaps = ['LtE','GtE','Lt','Gt']
					cumWeightOp = 0
					for element in opNodes:
						lineNum = element[0]
						operator = element[1]
						opsForSwap = copy.copy(opSwaps)
						if operator in opsForSwap:
							opsForSwap.remove(operator)
						if (source,lineNum) in self.treeWeights:
							rangeOp = self.treeWeights[(source,lineNum)]
							if rangeOp == 0:
								rangeOp = 0.005
						else:
							rangeOp = 0.005
						cumWeightOp += rangeOp
						self.opList.append([source,cumWeightOp,lineNum,operator,opsForSwap])

			if self.varList != None:
				#
				if LogKeeper.varWeights:
					LogKeeper.MasterLog.write_Log('VarWeightsList\n')
				##
				targets = []
				for x in xrange(0,len(self.varList)):
					self.varList[x][1] = self.varList[x][1]/cumWeightVar
					if (self.varList[x][3],self.varList[x][2]) not in targets:
						targets.append((self.varList[x][3],self.varList[x][2]))
					#
					if LogKeeper.varWeights:
						LogKeeper.MasterLog.write_Log(str(x)+','+str(self.varList[x])+'\n')
					##
				self.varList = [self.varList,targets]
			if self.opList != None:
				#
				if LogKeeper.opWeights:
					LogKeeper.MasterLog.write_Log('OpWeightsList\n')
				##	
				for x in xrange(0,len(self.opList)):
					self.opList[x][1] = self.opList[x][1]/cumWeightOp
					#
					if LogKeeper.varWeights:
						LogKeeper.MasterLog.write_Log(str(x)+','+str(self.opList[x])+'\n')
					##

	def getResults(self):
		return self.ASTList, self.opList, self.varList
