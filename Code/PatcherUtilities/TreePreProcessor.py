
from ast import *

class TreePreProcessor(object):
	
	def __init__(self):
		self.nodesPerLine = dict()
		self.nameCounter = 0
		self.curFile = None
	
	# Method that total number of nodes in the tree
	def countNodes(self,rootAst):
		count = 0
		for child in iter_child_nodes(rootAst):
			count += self.countNodes(child)
		return count + 1
	
	# lineNodeCount method obtains the number of nodes per line,
	# storing them in a dictionary maintained by the class instance itself.
	def lineNodeCount(self,file,rootAst):
		self.curFile = file
		curLine = 0
		self.lineNodeCountHelper(curLine,rootAst)
		return self.nodesPerLine
	
	# Recursive helper method for lineNodeCount
	def lineNodeCountHelper(self,curLine,rootAst):
		
		hasLineNum = getattr(rootAst, 'lineno', None)
		if hasLineNum is not None:
			curLine = rootAst.lineno
			if (self.curFile,rootAst.lineno) not in self.nodesPerLine:
				self.nodesPerLine[(self.curFile,rootAst.lineno)] = 1
			else:
				self.nodesPerLine[(self.curFile,rootAst.lineno)] += 1
		else:
			if (self.curFile,curLine) not in self.nodesPerLine:
				self.nodesPerLine[(self.curFile,curLine)] = 1
			else:
				self.nodesPerLine[(self.curFile,curLine)] += 1
			
		for child in iter_child_nodes(rootAst):
			self.lineNodeCountHelper(curLine,child)

	# method used for debuging print and traverse the entire tree for examination.
	def visitAll(self,rootAst):
		#if rootAst.__class__.__name__ == 'Name':
		for field in iter_fields(rootAst):
			print field
		for child in iter_child_nodes(rootAst):
			self.visitAll(child)
