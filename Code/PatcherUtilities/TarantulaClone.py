
# TarantulaClone
# ----------------------
# Assigns a value to line numbers in source code as they are executed by tests.
# This value is an indication oof how suspicious that particular line is.
# Values range from 0 to 1, 0 being least suspicious and 1 being most suspicious.
# Now written as a function that returns a table of relative line probabilities.
# Now written so deals with different input file location with respect to self
# ----------------------
# Author: Thomas Ackling
# ----------------------

import sys, os, trace, types, unittest
sys.path.insert(0,os.getcwd())
#import Code.Data_Writer.LogKeeper as LogKeeper

class DevNull:

	def write(self, str):
		pass
	def flush(self):
		pass
	def close(self):
		pass

class TarantulaClone(object):

	def __init__(self):
		self.loader = None
		self.runner = None
		self.tracer = None
		self.suite = None
		self.result = None

	def generate_Line_Weight_Table(self, modulePath, testFilePath, testFile, sourceFiles):
		"""
		returns a table of offsets for each node in a line as a function of file name and line number. 
		"""
		
		for file in sourceFiles:
			__import__(modulePath+file)
		
		__import__(modulePath+testFile)

		self.loader = unittest.TestLoader()
		self.runner = unittest.TextTestRunner(stream=DevNull())
		testNames = self.loader.getTestCaseNames(sys.modules[modulePath+testFile].TestSeries)
		
		# Declaring data space
		dataDict = dict()
		suspiciousness = dict()
		totalFailed = totalPassed = 0
		numOfTests = len(testNames)

		for name in testNames:
			# begin extracting information from each individual test
			self.suite = unittest.TestSuite()
			self.suite.addTest(sys.modules[modulePath+testFile].TestSeries(name))
			self.tracer = trace.Trace(ignoredirs=[sys.prefix, sys.exec_prefix],trace=0, count=1)
			
			# Run test under trace
			self.tracer.runctx(cmd="self.result = self.runner.run(self.suite)", globals=globals(), locals=locals())
			
			# Increment totalPassed or totalFailed
			overallResult = self.result.wasSuccessful() # overallResult = True if test passed
			if overallResult:
				totalPassed += 1
			else:
				totalFailed += 1
			
			# Search tracer output for executed lines from source code
			for a in self.tracer.counts:
				for source in sourceFiles:
					if a[0].find(source) >= 0:
						lineNum = a[1]	
						key = source, lineNum
						if key not in dataDict:
							if overallResult:
								dataDict[key] = 1,0
							else:
								dataDict[key] = 0,1
						else:
							if overallResult:
								dataDict[key] = 1+dataDict[key][0],dataDict[key][1]
							else:
								dataDict[key] = dataDict[key][0],1+dataDict[key][1]
		
		# Calculate suspiciousness values.
		for key in dataDict:
			if totalFailed is 0:
				suspiciousness[key] = 0
			elif totalPassed is 0:
				numer = (dataDict[key][1]/float(totalFailed))
				denom = (dataDict[key][1]/float(totalFailed))
				suspiciousness[key] = numer/denom
			else:
				numer = (dataDict[key][1]/float(totalFailed))
				denom = ((dataDict[key][1]/float(totalFailed)) + (dataDict[key][0]/float(totalPassed)))
				suspiciousness[key] = numer/denom

		sum = 0
		# Get relative probability scores 
		for key in suspiciousness:
			sum += suspiciousness[key]
			
		# No tests were failed
		if sum is 0:
			rtnList = [suspiciousness,numOfTests,]
			return rtnList
		
		for key in suspiciousness:
			suspiciousness[key] = suspiciousness[key]/sum
		
		rtnList = [suspiciousness,numOfTests]
		return rtnList
