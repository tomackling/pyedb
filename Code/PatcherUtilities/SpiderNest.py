
from TarantulaClone import *

def factorialTarantula():
	modulePath = 'Artifical_Benchmarks.Factorial.'
	testPath = 'C:\\Users\\Thomas\\Summer Research\\Code\\Artifical_Benchmarks\\Factorial\\'
	testFile = 'TestFactorial'
	sourceFiles = []
	sourceFiles.append('factcalc')
	table = TarantulaClone().generate_Line_Weight_Table(modulePath, testPath, testFile, sourceFiles)

	for a in table:
		print a[0],a[1],'->',table[a]

def summerTarantula():
	modulePath = 'Artifical_Benchmarks.Summer.'
	testPath = 'C:\\Users\\Thomas\\Summer Research\\Code\\Artifical_Benchmarks\\Summer\\'
	testFile = 'TestSummer'
	sourceFiles = []
	sourceFiles.append('summer')
	sourceFiles.append('summerpt2')
	table = TarantulaClone().generate_Line_Weight_Table(modulePath, testPath, testFile, sourceFiles)

	for a in table:
		print a[0],a[1],'->',table[a]
		
summerTarantula()