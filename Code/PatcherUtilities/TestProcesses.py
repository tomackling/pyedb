
import sys, os, types, unittest
sys.path.insert(0,os.getcwd())
#from Code.Data_Writer.LogKeeper import LogKeeper as Keeper
from multiprocessing import Queue
from multiprocessing import Process
from time import clock

class DevNull:

	def write(self, str):
		pass
	def flush(self):
		pass
	def close(self):
		pass

def testProcess(q):
	
	args = q.get()
	testFilePath = args['testFilePath']
	testFile = args['testFile']
	individual = args['ind']
	
	for key in individual.myASTList:
		newModule = types.ModuleType(key, 'compiled from ' + str(individual))
		code = compile(individual.myASTList[key], key, 'exec')
		exec code in newModule.__dict__
		sys.modules[key] = newModule
	
	testModule = types.ModuleType('testSuite', 'compiled tests') # FIX IF module name in ast that is testSuite THIS WILL BREAK
	code = compile(open(testFilePath+testFile+'.py').read(), testFile, 'exec')
	exec code in testModule.__dict__
	
	loader = unittest.TestLoader()
	suite = loader.loadTestsFromModule(testModule)
	
	#
	#if LogKeeper.fitnessTest:
	#	log.write_Log('fitnessTest for '+individual.genotype+'\n\n')
	#	runner = unittest.TextTestRunner(stream=log.get_Stream(), verbosity = 1)
	#else:
	runner = unittest.TextTestRunner(stream=DevNull())
	##
	
	result = runner.run(suite)
	fitness = len(result.failures) + len(result.errors)
	
	q.put(fitness)
		
def TaranTestProcess(q):
	
	start = clock()
	#Retriving Required Information
	args = q.get()
	spiderNest = args['spiderNest']
	modulePath = args['modulePath']
	filePath = args['filePath']
	testFile = args['testFile']
	sourceFiles = args['sourceFiles']
	#Running Tarantula
	result = spiderNest.generate_Line_Weight_Table(modulePath, filePath, testFile, sourceFiles)
	table = result[0]
	numOfTests = result[1]
	#Sending Results
	end = clock()
	taranTime = end-start
	q.put([table,taranTime, numOfTests])
