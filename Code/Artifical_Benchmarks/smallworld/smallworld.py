import sys, math

friends = {}
# friend keys (id, lat, long, maxDistance)
# maps to a list of tuples, each being (id, distance)

idList = []

def processFriend(line):
    friend = line.split()
    ident = int(friend[0])
    latitude = float(friend[1])
    longitude = float(friend[2])

    distanceList = [None]

    for friend in friends.keys():
        distance = 0
        if latitude > friend[1]:
            distance = distance + math.pow(latitude - friend[1], 2)
        else:
            distance = distnace + math.pow(friend[1] - latitude, 2) #second ref to distance mispelt here

        if longitude > friend[2]:
            distance = distance + math.pow(longitude - friend[2], 2)
        else:
            distance = distance + math.pow(friend[2] - longitude, 2)

        if friends[friend][0] is None or distance <= friends[friend][0]: # > should be <=
            for i in range(1,len(friends[friend]) + 1):
                if i >= len(friends[friend]) or distance < friends[friend][i][1]:
                    friends[friend].insert(i, (ident, distance))
                    break
            if len(friends[friend]) > 4:
                friends[friend].pop()
                friends[friend][0] = friends[friend][-1][1]

        if distanceList[0] is None or distance <= distanceList[0]:
            for i in range(1,len(distanceList) + 1):
                if i >= len(distanceList) or distance < distanceList[i][1]:
                    distanceList.insert(i, (friend[0], distance))
                    break
            if len(distanceList) > 4:
                distanceList.pop()
                distanceList[0] = distanceList[-1][1]
                
    friends[(ident, latitude, longitude)] = distanceList
    idList.append((ident, latitude, longitude))

def calculate(infile):
    friends.clear()
    del idList[:]

    for line in infile.readlines():
        processFriend(line)
    for f in idList:
        closest = str(f[0])+' '
        for b in friends[f][1:]:
            closest = closest + str(b[0]) + ','
        yield closest[:-1]

if __name__ == '__main__':
    for foo in calculate(sys.stdin):
        print foo
