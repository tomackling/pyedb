import unittest, time, os
import smallworld

class TestSeries(unittest.TestCase):

    def testGiven(self):
        inFile = open(os.getcwd()+"/Code/Artifical_Benchmarks/smallworld/input1.txt")
        outFile = open(os.getcwd()+"/Code/Artifical_Benchmarks/smallworld/output1.txt")
        list = []
        for foo in smallworld.calculate(inFile):
            list.append(foo + '\n')
            self.assertEqual(foo + '\n', outFile.readline())
        self.assertNotEqual(len(list), 0)
        inFile.close()
        outFile.close()

    def testSmall(self):
        inFile = open(os.getcwd()+"/Code/Artifical_Benchmarks/smallworld/input5.txt")
        outFile = open(os.getcwd()+"/Code/Artifical_Benchmarks/smallworld/output5.txt")
        list = []
        for foo in smallworld.calculate(inFile):
            list.append(foo + '\n')
            self.assertEqual(foo + '\n', outFile.readline())
        self.assertNotEqual(len(list), 0)
        inFile.close()
        outFile.close()

    def testReversed(self):
        inFile = open(os.getcwd()+"/Code/Artifical_Benchmarks/smallworld/input6.txt")
        outFile = open(os.getcwd()+"/Code/Artifical_Benchmarks/smallworld/output6.txt")
        list = []
        for foo in smallworld.calculate(inFile):
            list.append(foo + '\n')
            self.assertEqual(foo + '\n', outFile.readline())
        self.assertNotEqual(len(list), 0)
        inFile.close()
        outFile.close()

    def test100(self):
        inFile = open(os.getcwd()+"/Code/Artifical_Benchmarks/smallworld/input2.txt")
        outFile = open(os.getcwd()+"/Code/Artifical_Benchmarks/smallworld/output2.txt")
        list = []
        for foo in smallworld.calculate(inFile):
            list.append(foo + '\n')
            self.assertEqual(foo + '\n', outFile.readline())
        self.assertNotEqual(len(list), 0)
        inFile.close()
        outFile.close()

    def test1000(self):
        inFile = open(os.getcwd()+"/Code/Artifical_Benchmarks/smallworld/input3.txt")
        outFile = open(os.getcwd()+"/Code/Artifical_Benchmarks/smallworld/output3.txt")
        list = []
        for foo in smallworld.calculate(inFile):
            list.append(foo + '\n')
            self.assertEqual(foo + '\n', outFile.readline())
        self.assertNotEqual(len(list), 0)
        inFile.close()
        outFile.close()

if __name__ == "__main__":
    unittest.main()
