from math import *
import os
import random
import sys
VERBOSE=True

class GenotypeToModel:

    ''' takes a bit-string of arbitrary length and produces a model from it '''

    # class attributes.. should really qualify refs to these with "GenotypeToModel..." but "self" is shorter
    # constants 
    BITS_FOR_LOC = 6             #number of bits for a location
    BITS_FOR_SCALE = 5           #number of bits for scale
    BITS_FOR_ROT = 5             #number of bits in a rotation
    BITS_FOR_INTENSITY = 5       #number of bits in the central intensity
    BITS_FOR_ATTENUATION = 4     #number of bits in the attenuation power

    #total number of bits to make up one function in the genome
    TOTAL_BITS = BITS_FOR_LOC*3 + BITS_FOR_SCALE*3 + BITS_FOR_ROT*3 + BITS_FOR_INTENSITY + BITS_FOR_ATTENUATION

    #boundaries in bit-list describing one function in the genome.
    BOUNDARIES = [BITS_FOR_LOC,BITS_FOR_LOC,BITS_FOR_LOC,
                  BITS_FOR_SCALE,BITS_FOR_SCALE,BITS_FOR_SCALE,
                  BITS_FOR_ROT, BITS_FOR_ROT, BITS_FOR_ROT,
                  BITS_FOR_INTENSITY, BITS_FOR_ATTENUATION]

    #model boundaries.. bits are normalised on linear scale to fit in these bounds
    LOC_LOWER = 0.0     #location bounds
    LOC_UPPER = 1.0
    LOC_RANGE = LOC_UPPER-LOC_LOWER
    SCALE_LOWER = 0.02  #scaling bounds
    SCALE_UPPER = 0.8
    SCALE_RANGE = SCALE_UPPER-SCALE_LOWER
    ROT_LOWER = 0.0     #rotational bounds ... in radians
    ROT_UPPER = 6.28
    ROT_RANGE = ROT_UPPER-ROT_LOWER
    INTENSITY_LOWER = -0.4   #intensity bounds half-space is zero
    INTENSITY_UPPER = 0.6
    INTENSITY_RANGE = INTENSITY_UPPER-INTENSITY_LOWER
    ATTENUATION_LOWER = 0.2  # attenuation bounds.. lower == very blurry upper== sharpish edge
    ATTENUATION_UPPER = 4.5
    ATTENUATION_RANGE = ATTENUATION_UPPER-ATTENUATION_LOWER

    #model space boundaries (for all  x,y,z directions)
    #outside of these boundaries won't be rendered into the model
    MODEL_SPACE_MIN = 0.25
    MODEL_SPACE_MAX = 0.75

    #conversion parameters to log scale.. in this model 0.0 in the intensity = half-space.
    #we need then to
    # 1) set the half-space (in log_10 terms)
    # 2) set the log_10 interval..
    # so, for example if INTENSITY_LOWER=-0.4 and INTENSITY_UPPER=0.6, HALF_SPACE=2.0 and
    #                    LOG10_INTERVAL=0.1
    # we have minimum value 10-2 ohms, maximum value 10^8 ohms.
    
    HALF_SPACE=2.0  #the 10^HALF_SPACE is our half-space interval .. this is always at an intensity
                    #value of 0.0
    LOG10_INTERVAL=0.2 # every 0.2 change in intensity is a multiplicative factor of 10 in
                       # resistivity.

    #sampled rendering of functions for testing and viewing
    lPower=5.0  # Lp power for generalised average for combining samples - must be odd
                # whole number to cater for negatives
    
    def __init__(self,
                 genomeString,       # bit string representing individual
                 xints,yints,zints):      # raw dimension arrays
        self.S=[]        # array of sample locations.
        self.xints=xints    #preserve raw intervals
        self.yints=yints
        self.zints=zints
        self.xds=self.normaliseIntervals(xints)     # normalise and save array for later model printing
        self.yds=self.normaliseIntervals(yints)     # same
        self.zds=self.normaliseIntervals(zints)

        
        self.initSampleLocations(self.xds,self.yds,self.zds)  #initialise the array of sample location with dimension arrays
        self.xSamps = len(xints)
        self.ySamps = len(yints)
        self.zSamps = len(zints)

        self.M = []      # array to hold samples
        self.funlist=[]            # empty fun list
        remaining=genomeString  #initialise from genome string
        remaining=strcompress(remaining) #get rid of any whitespace

        #now to build the fun-list
        #while there is at least one genein remaining genome
        while (len(remaining)>=self.TOTAL_BITS):  
            #take one gene off the front of the genome
            first=remaining[0:self.TOTAL_BITS]
            #and leave remainder
            remaining = remaining[self.TOTAL_BITS:]
            # build a function from the next gene
            self.funlist.append(self.bitsToFun(first))


    # class method to intialise the sample array at a fixed number of regular intervals
    # in x,y and z direction..
    @classmethod
    def simpleInit(cls,genomeString,samples):
        #simple initialisation at same regular intervals in x, y and z dimensions
        xds=[]
        yds=[]
        zds=[]
        for i in range(0,samples):
            xds.append(1.0)  # have a dummy interval of 1.0
            yds.append(1.0)  
            zds.append(1.0)
        return GenotypeToModel(genomeString,xds,yds,zds)


    #initialise from a model file
    @classmethod
    def modelInit(cls,genomeString,modIntFile):

        try:
            fob=open(modIntFile, 'r') # try to open file for reading
        except IOError as e:                # fail? ...complain
            print("file access error")
            raise
        else:                           # succeed? ... keep reading
            fob.readline()              # skip the first line .. it is rubbish..
            s=fob.readline()            # read in the dims
            dims=s.split()              # split into four
            xDim=int(dims[0])
            yDim=int(dims[1])
            zDim=int(dims[2])
            #print('xyz dims' + str(xDim) + ' ' + str(yDim) + ' ' + str(zDim))
            #initialise interval arrays and sums
            allDims=[]
            totalDims=xDim+yDim+zDim
            #now read in all the intervals
            intCount=0
            while (intCount < totalDims):
                s=fob.readline()
                dims=s.split()
                for i in range(0,len(dims)):
                    allDims.append(float(dims[i]))  # add value to array of dimensions
                    intCount+=1              # increment total count
                    if intCount >= totalDims: # if we are at the end
                        break #finish
                    
            # now allDims has all of the dimension values.. slice it
            xDims = allDims[0:xDim]
            yDims = allDims[xDim:xDim+yDim]
            zDims = allDims[xDim+yDim:xDim+yDim+zDim]

            # now we have arrays of dimensions... as first stage of normalisation
            # work out the extent of each dimension by summing these arrays..
            xExtent = sum(xDims)
            yExtent = sum(yDims)
            zExtent = sum(zDims)

            return GenotypeToModel(genomeString,xDims,yDims,zDims)

    #normalise the intervals array so that it fits into our standard
    #sample space 
    def normaliseIntervals(self,intervals):
        # a factory for a normalisation function for the intervals
        def normaliserFactory(extent):
            def normalise(val):
                return val * ((self.MODEL_SPACE_MAX-self.MODEL_SPACE_MIN)/extent)
            return normalise

        modelExtent = sum(intervals)  # find the length spanned by all intervals

        # use map to map raw intervals to corresponding intervals in the
        # standardised model space (this is a linear transform)

        res = list(map(normaliserFactory(modelExtent),intervals))
        return res
    

    
    # create a long array of sample locations from an array of intervals
    def initSampleLocations(self,xds,yds,zds):
        count=0                            #global sample array index
        currz=self.MODEL_SPACE_MIN+(zds[0]/2.0) # start midway through first interval
        for i in range(0,len(zds)):        # for all the z spacings
            curry=self.MODEL_SPACE_MIN+(yds[0]/2.0) # start midway through first interval
            for j in range(0,len(yds)):     # for all the y spacings
                currx=self.MODEL_SPACE_MIN+(xds[0]/2.0) # start midway through first interval
                for k in range(0,len(xds)):   # for all the x spacings
                    self.S.append([currx,curry,currz])  # update the array S
                    currx += xds[k]                # add the interval
                curry += yds[j]
            currz += zds[i]

            #post-condition S initialised with intervals..
            #print(self.S)
                    

    #converts the bits expressing an individual function into that function
    def bitsToFun(self,funString):
        funBits=[] #list of lists of function parts (substrings of funString)
        #now split at boundaries and put pieces in geneBits
        currIndex=0;
        for i in range(0,len(self.BOUNDARIES)):
            nextIndex=currIndex+self.BOUNDARIES[i]
            funBits.append(funString[currIndex:nextIndex])
            currIndex=nextIndex

        #turn the bit representations into ints
        funInts = list(map(self.bitsToInt,funBits))
        #and then into floats normalised to the appropriate ranges
        funFloats = self.normalise(funInts)
        #print(funFloats)

        #create parametric function from floating point parameter

        res=self.floatsToFun(funFloats)
        #print(res)
        return res


    #converts a single gene string into an integer
    def bitsToInt(self,geneString):
        return int(geneString,2)


    # turns integers into floating points within the range of our model...
    def normalise(self,geneInt):
        #the code below is fairly boiler-plate I'm afraid.. just to do with the encoding
        # of the model..
        res=[]
        res.append(self.LOC_LOWER+(geneInt[0]/pow(2.0,self.BITS_FOR_LOC))*self.LOC_RANGE)
        res.append(self.LOC_LOWER+(geneInt[1]/pow(2.0,self.BITS_FOR_LOC))*self.LOC_RANGE)
        res.append(self.LOC_LOWER+(geneInt[2]/pow(2.0,self.BITS_FOR_LOC))*self.LOC_RANGE)
        res.append(self.SCALE_LOWER+(geneInt[3]/pow(2.0,self.BITS_FOR_SCALE))*self.SCALE_RANGE)
        res.append(self.SCALE_LOWER+(geneInt[4]/pow(2.0,self.BITS_FOR_SCALE))*self.SCALE_RANGE)
        res.append(self.SCALE_LOWER+(geneInt[5]/pow(2.0,self.BITS_FOR_SCALE))*self.SCALE_RANGE)
        res.append(self.ROT_LOWER+(geneInt[6]/pow(2.0,self.BITS_FOR_ROT))*self.ROT_RANGE)
        res.append(self.ROT_LOWER+(geneInt[7]/pow(2.0,self.BITS_FOR_ROT))*self.ROT_RANGE)
        res.append(self.ROT_LOWER+(geneInt[8]/pow(2.0,self.BITS_FOR_ROT))*self.ROT_RANGE)
        res.append(self.INTENSITY_LOWER+(geneInt[9]/pow(2.0,self.BITS_FOR_INTENSITY))*self.INTENSITY_RANGE)
        res.append(self.ATTENUATION_LOWER+(geneInt[10]/pow(2.0,self.BITS_FOR_ATTENUATION))*self.ATTENUATION_RANGE)
        return res


    #takes a list of params and returns a parametric function
    #this function returns a volumetric function which returns,
    #for each x,y,z position an intensity which represents a deviation
    #from the half-space of the model.. this gets scaled to a log value
    #in the render-to-samples function
    def floatsToFun(self,params):
        #locations
        cx=params[0]
        cy=params[1]
        cz=params[2]
        #scales
        sx=params[3]
        sy=params[4]
        sz=params[5]
        #rotations
        xrot=params[6]
        yrot=params[7]
        zrot=params[8]
        #max intensity
        max=params[9]
        #power attenuation
        power=params[10]

        #now define a local function
        def cloudPlot(x,y,z):
            #do rotational
            #and scaling transforms to values and return
            #plot point

            #measure each point relative to the centre (translate)
            # put code here...
            xt=x-cx
            yt=y-cy
            zt=z-cz
        
            #now xt,xy,xz is the measure of x,y,z rel to centre of spheroid
            #now rotate these points about the centre of the spheroid
            #rot about z
            xtr=xt*cos(-zrot)-yt*sin(-zrot)
            ytr=xt*sin(-zrot)+yt*cos(-zrot)
            ztr=zt
            #rot about x
            xtrr=xtr
            ytrr=ytr*cos(-xrot) - ztr*sin(-xrot)
            ztrr=ytr*sin(-xrot) + ztr*cos(-xrot)
            #rot about y
            xtrrr=ztrr*sin(-yrot) + xtrr*cos(-yrot)
            ztrrr = ztrr*cos(-yrot) - xtrr*sin(-yrot)
            ytrrr = ytrr

            #scale the points (inverse to the shape scale)

            xtrrrs = xtrrr * (1/sx)
            ytrrrs = ytrrr * (1/sy)
            ztrrrs = ztrrr * (1/sz)

            #translate point and then plot
            #put code here..

            #take translation and scaling out of code below
            return max/(pow(pow(xtrrrs,2)+
                            pow(ytrrrs,2)+
                            pow(ztrrrs,2),power)+1)
        return cloudPlot

    #convert to log10 values of resistivity
    def convertToLog10Value(self,v):
        return (v+self.HALF_SPACE*self.LOG10_INTERVAL)*(1.0/self.LOG10_INTERVAL)

    # render the functions that were built in the bitstring to an array for later dumping
    # and display..
    def renderToSamples(self):

        # allocate 
        self.M=[0]*(len(self.S)) #alloc enough space to hold the sample cube

        
        maxSoFar=-100000000000
        minSoFar=1000000000000
        #for every function for every sample
        numFuncs = len(self.funlist)
        for n in range(len(self.M)):   
            accum = self.M[n] # initialise with background value
            for f in range(numFuncs):
                #sample the functions at the current point
                xval=self.S[n][0]
                yval=self.S[n][1]
                zval=self.S[n][2]
                funVal= self.funlist[f](xval,yval,zval)
                accum += pow(funVal,self.lPower)/numFuncs  #accum terms in generalised norm
            
            #combine using generalised average and convert to log of Resistance.
            self.M[n]= self.convertToLog10Value(nth_root(accum,self.lPower))
            maxSoFar=max(maxSoFar,self.M[n])
            minSoFar=min(minSoFar,self.M[n])
                
            # debug.. print out samples
            #print(self.M[n])

    def getSamples(self):
        return self.M;

    def save(self,filename):

        def toUnitRange(v):
            max=self.INTENSITY_UPPER/self.LOG10_INTERVAL+self.HALF_SPACE
            min=self.INTENSITY_LOWER/self.LOG10_INTERVAL+self.HALF_SPACE
            return 1.0-((v-min)/(max-min)) #normalise to zero to one and flip the sense so conductive is brightly displayed..
        
        if os.access(filename,os.F_OK): return False # Don't overwrite!
        else:
            voxels=list(map(toUnitRange,self.M)) #make M displayable
            fob = open(filename + '.data','w')
            fob.write(str(self.xSamps))
            fob.write('\n')
            fob.write(str(self.ySamps))
            fob.write('\n')
            fob.write(str(self.zSamps))
            fob.write('\n')
            fob.write(str(voxels))
            fob.close()
            return True

    def saveModel(self,filename):
        #inverse of log 10 of number
        def inverseLog10(v):
            return power(10,v)
     
        fob = open(filename,'w')
        # write header
        fob.write('#Artificial Data')
        fob.write('\n')
        #write number of samples plus dummy zero
        fob.write('  ' + str(self.xSamps) + '  ' + str(self.ySamps) + '  ' + str(self.zSamps) + '   0')
        fob.write('\n')
        #write intervals, seven per line
        allInts=self.xints + self.yints + self.zints  # concatenate all intervals together.
        intGroups = group(allInts,7)   # group into intervals of seven
        for i in range(len(intGroups)):  # for each line
            for j in range(len(intGroups[i])): # for each interval
                fob.write(str(intGroups[i][j])+ ' ')
            fob.write('\n')

        for i in range(len(self.M)):
            fob.write(str(pow(10,self.M[i])))
            fob.write('\n')
        fob.close()
        return True

        

    def readModelIntervals(self,modIntFile):
        try:
            fob=open(modIntFile, 'r') # try to open file for reading
        except IOError as e:                # fail? ...complain
            if e.errno == errno.EACCESS:
                print("file access error")
                raise
        else:                           # succeed? ... keep reading
            fob.readline()              # skip the first line .. it is rubbish..
            s=fob.readline()            # read in the dims
            dims=s.split()              # split into four
            #print(dims)
            xDim=int(dims[0])
            yDim=int(dims[1])
            zDim=int(dims[2])
            #print('xyz dims' + str(xDim) + ' ' + str(yDim) + ' ' + str(zDim))
                
                    

# finds the nth root of a number.. intended to be used on
# whole, odd-root powers.
def nth_root(x,n):
    return copysign(pow(abs(x), 1.0/n), x)

#utility function that strips all spaces from a string
# credit to Tim Michaelson
def strcompress(inp):
    res = ''.join(inp.split())
    return res

#from python forums (author anonymous)
#splits a sequence s into chunks of size n

def group(s, n):
    #print(len(s))
    return [s[i:i+n] for i in xrange(0, len(s), n)]

#res=GenotypeToModel('01110111001010101010101010100100101010101010101010101010010101110101010101010101010101010010110100110')
#res=GenotypeToModel('000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000')
#res=GenotypeToModel('111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111')
#res=GenotypeToModel('100000100000100000000010000100001000000000000000000011111')
#res=GenotypeToModel('100000 100000 100000 00111 00111 00111 00000 00000 00000 00000 1111    100000 100000 100000 00001 00000 00000 00100 00100 00100 11111 1111   ')
#res=GenotypeToModel('100000 100000 100000 00111 00111 00111 00000 00000 00000 00000 1111 ')
#res=GenotypeToModel('100000 100000 010000 01000 01000 00000 00000 00000 00000 00000 1000     100000 100000 100000 00100 00010 00001 00100 00100 00010 00001 1111   100000 100000 101000 00100 00110 00001 00000 00110 00010 00100 1111    100000 100000 110010 10000 10010 00011 00001 00001 00001 11111 1000 ')
#res=GenotypeToModel.simpleInit('100000 100000 010000 01000 01000 00000 00000 00000 00000 00000 1000     100000 100000 100000 00100 00010 00001 00100 00100 00010 00001 1111   100000 100000 101000 00100 00110 00001 00000 00110 00010 00100 1111    100000 100000 110010 10000 10010 00011 00001 00001 00001 11111 1000 ', 20)
#res.renderToSamples()
#res.save('genotype')
#res=GenotypeToModel.modelInit('100000 100000 010000 01000 01000 00000 00000 00000 00000 00000 1000     100000 100000 100000 00100 00010 00001 00100 00100 00010 00001 1111   100000 100000 101000 00100 00110 00001 00000 00110 00010 00100 1111    100000 100000 110010 10000 10010 00011 00001 00001 00001 11111 1000 ',
#                              'blk3d_model.01_00')
#res=GenotypeToModel.modelInit('100000 100000 010000 01000 01000 00000 00000 00000 00000 00000 1000     100000 100000 100000 00100 00010 00001 00100 00100 00010 00001 1111   100000 100000 101000 00100 00110 00001 00000 00110 00010 00100 1111    100000 100000 110010 10000 10010 00011 00001 00001 00001 11111 1000 ',
#                              'uniform_model.00')
#res.renderToSamples()
#res.save('testmodel')
#res.saveModel('testmodel.00')

#res=GenotypeToModel.modelInit('100000 100000 010000 00000 00000 01000 00000 00000 00000 00000 1111  100000 100000 010000  00110 00110 11010 00000 00000 00000 10001 0001 100000 100000 100000 10000 10000 10000 00000 00000 00000 01001 0100 ','blk3d_model.01_00')
#res.renderToSamples()
#res.saveModel('testmodel.01')

#res.readModelIntervals('blk3d_model.01_00')


### Main routine..
### check number of command line args
#if len(sys.argv)!=4:
#    print('usage: genotype_to_model  <inp_model_file> <out_model_file> <genome_string>')
    #print(sys.argv[3])
#    sys.exit(1)

#inp_model_file=sys.argv[1]
#out_model_file=sys.argv[2]
#genome_string=sys.argv[3]

#res=GenotypeToModel.modelInit(genome_string,inp_model_file) # call factory
#res.renderToSamples()                 # render model at intervals from inp_model
#res.saveModel(out_model_file)         # blat render to model file.



res=GenotypeToModel.simpleInit('000000 000000 000000 00000 00000 00000 00000 00000 00000 00000 0000', 1)
res.renderToSamples()
testVal = res.getSamples()
print(testVal)

