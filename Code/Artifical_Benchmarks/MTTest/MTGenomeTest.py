import unittest
import GenotypeToModel

class TestSeries(unittest.TestCase):
	def testOne(self):
		res=GenotypeToModel.GenotypeToModel.simpleInit('100000 100000 110000 11100 10100 00100 00000 00000 00000 11111 1111', 2)
		res.renderToSamples()
		testVal = res.getSamples()
		self.assertEqual(testVal, [2.0001486765799119, 2.0001486765799119, 2.0001486765799119, 2.0001486765799119, 2.8482645258139971, 2.8482645258139971, 2.8482645258139971, 2.8482645258139971])
		
	def testTwo(self):
		res=GenotypeToModel.GenotypeToModel.simpleInit('100000 100000 100000 11100 10100 00100 00000 00000 00000 11111 1111', 2)
		res.renderToSamples()
		testVal = res.getSamples()
		self.assertEqual(testVal, [2.8482645258139971, 2.8482645258139971, 2.8482645258139971, 2.8482645258139971, 2.8482645258139971, 2.8482645258139971, 2.8482645258139971, 2.8482645258139971])
		
	def testThree(self):
		res=GenotypeToModel.GenotypeToModel.simpleInit('100000 100000 100000 11100 10100 00100 00000 10000 00000 11111 1111', 2)
		res.renderToSamples()
		testVal = res.getSamples()
		self.assertEqual(testVal, [2.8555023298097817, 2.8410825909275621, 2.8555023298097817, 2.8410825909275621, 2.8410825909275621, 2.8555023298097817, 2.8410825909275621, 2.8555023298097817])
		
	def testFour(self):
		res=GenotypeToModel.GenotypeToModel.simpleInit('100000 100000 100000 11100 10100 00100 00000 10000 00000 11111 1111', 1)
		res.renderToSamples()
		testVal = res.getSamples()
		self.assertEqual(testVal,[4.84375])
		
	def testFive(self):
		res=GenotypeToModel.GenotypeToModel.simpleInit('100000 100000 100000 10000 10000 10000 00000 10000 00000 00000 1110', 1)
		res.renderToSamples()
		testVal = res.getSamples()
		self.assertEqual(testVal,[0.0])
		
	def testSix(self):
		res=GenotypeToModel.GenotypeToModel.simpleInit('', 1)
		res.renderToSamples()
		testVal = res.getSamples()
		self.assertEqual(testVal,[2.0])
		
	def testSeven(self):
		res=GenotypeToModel.GenotypeToModel.simpleInit('101100 111111 100010 00001 00011 01000 00100 10000 01000 11111 0001', 2)
		res.renderToSamples()
		testVal = res.getSamples()
		self.assertEqual(testVal,[2.2225072175805085, 2.2240383593841342, 2.3422589044802731, 2.3483605882466891, 2.2195452019072768, 2.2239282823599376, 2.3305058477788183, 2.3477411896799296])
		
	def testEight(self):
		res=GenotypeToModel.GenotypeToModel.modelInit('100000 100000 10000 01000 01000 00000 00000 00000 00000 10000 1000 111111111111',
                            'blk_model.01')

		res.renderToSamples()
		testVal = res.getSamples()
		self.assertEqual(testVal,[1.7425025511978878, 1.7422466433842687, 1.6178644926928727, 1.6168945943086102, 1.8421301337769518, 1.8420786269226319])
	def testNine(self):
		res=GenotypeToModel.GenotypeToModel.modelInit('100001 100010 101010 00001 00010 00100 10000 01000 00100 11111 1111',
                            'blk_model.01')
		res.renderToSamples()
		testVal = res.getSamples()
		self.assertEqual(testVal,[2.0000002075597005, 2.0000003057057949, 2.0007447985741429, 2.0347580589938281, 2.0002935519659499, 2.0044925366627488])
		
		
if __name__ == '__main__':
	unittest.main()

