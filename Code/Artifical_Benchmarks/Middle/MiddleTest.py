import unittest
import middle

class TestSeries(unittest.TestCase):
	def testOne(self):
		testVal = middle.middleFunc(3,3,5)
		self.assertEqual(testVal, 3)
	def testTwo(self):
		testVal = middle.middleFunc(1,2,3)
		self.assertEqual(testVal, 2)
	def testThree(self):
		testVal = middle.middleFunc(3,2,1)
		self.assertEqual(testVal, 2)
	def testFour(self):
		testVal = middle.middleFunc(5,5,5)
		self.assertEqual(testVal, 5)
	def testFive(self):
		testVal = middle.middleFunc(5,3,4)
		self.assertEqual(testVal, 4)
	def testSix(self):
		testVal = middle.middleFunc(2,1,3)
		self.assertEqual(testVal, 2)
	def testSeven(self):
		testVal = middle.middleFunc(1,3,2)
		self.assertEqual(testVal, 2)
	def testEight(self):
		testVal = middle.middleFunc(2,3,1)
		self.assertEqual(testVal, 2)
		
if __name__ == '__main__':
	unittest.main()

