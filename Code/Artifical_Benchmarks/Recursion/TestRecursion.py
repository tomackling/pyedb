
from unittest import *
from Recursion import *

class TestSeries(TestCase):
	
	def testBase(self):
		res = fib(0)
		self.assertEqual(res, 1)
		res = fib(1)
		self.assertEqual(res, 1)
	
	def test2(self):
		res = fib(2)
		self.assertEqual(res, 2)
		
	def test4(self):
		res = fib(4)
		self.assertEqual(res, 5)