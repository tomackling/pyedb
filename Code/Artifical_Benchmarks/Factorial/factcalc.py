def factorial(m):
	i = 1
	result = 1
	while i > m:
		result = result * i 
		i += 1
	return result
