import unittest
import factcalc

class TestSeries(unittest.TestCase):

	def testFive(self):
		result = factcalc.factorial(5)
		self.assertEqual(result, 120)
	def testTen(self):
		result = factcalc.factorial(10)
		self.assertEqual(result, 3628800)
	def testZero(self):
		result = factcalc.factorial(0)
		self.assertEqual(result, 1)
	def testOne(self):
		result = factcalc.factorial(1)
		self.assertEqual(result, 1)
		
if __name__ == '__main__':
    unittest.main()
