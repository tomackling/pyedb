# Microwave class
# States:
# 0 - Not Ready
# 1 - Ready
# 2 - Cooking
# 3 - Door Open
# 4 - Safety

class Microwave:
	def __init__(self):
		self.state = 0

	def action(self, inCommand):
		if self.state == 0:
			if inCommand == 'T':
				self.state = 7
				return "Timer Set"
			elif inCommand == 'O':
				self.state = 3
				return "Door Opened"
			elif inCommand in ('C', 'R', 'S', 'H', 'E'):
				return "Operation not permitted"
			else:
				return "Command not recognized"
		elif self.state == 1:
			if inCommand == 'S':
				self.state = 2
				return "Cooking"
			elif inCommand == 'O':
				self.state = 4
				return "Door Open"
			elif inCommand == 'R':
				self.state = 0
				return "Timer Reset"
			elif inCommand in ('C', 'T', 'H', 'E'):
				return "Operation not permitted"
			else:
				return "Command not recognized"
		elif self.state == 2:
			if inCommand == 'E':
				self.state = 0
				return "Timer Expired"
			elif inCommand == 'O':
				self.state = 4
				return "Door opened - Cooking Stopped!"
			elif inCommand == 'H':
				self.state = 1
				return "Cooking Paused"
			elif inCommand in ('C', 'R', 'S', 'T'):
				return "Operation not permitted"
			else:
				return "Command not recognized"
		elif self.state == 3:
			if inCommand == 'C':
				self.state = 0
				return "Door Closed"
			elif inCommand in ('O', 'R', 'S', 'H', 'E', 'T'):
				return "Operation not permitted"
			else:
				return "Command not recognized"
		elif self.state == 4:
			if inCommand == 'C':
				self.state = 1
				return "Door Closed"
			elif inCommand in ('O', 'R', 'S', 'H', 'E', 'T'):
				return "Operation not permitted"
			else:
				return "Command not recognized"
		else:
			return "FSM Broken!"
	def getState(self):
		if self.state == 0:
			return "NOT_READY"
		elif self.state == 1:
			return "READY"
		elif self.state == 2:
			return "COOKING"
		elif self.state == 3:
			return "DOOR_OPEN"
		elif self.state == 4:
			return "SAFETY"
