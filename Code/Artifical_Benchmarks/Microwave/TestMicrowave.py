import unittest
import microwave

class TestSeries(unittest.TestCase):
	def setUp(self):
		self.oven = microwave.Microwave()
	def testInitialState(self):
		result = self.oven.getState()
		self.assertEqual(result, "NOT_READY")
	def testOpenState(self):
		result = self.oven.action('O')
		self.assertEqual(result, "Door Opened")

		result = self.oven.getState()
		self.assertEqual(result, "DOOR_OPEN")

		result = self.oven.action('C')
		self.assertEqual(result, "Door Closed")

		result = self.oven.getState()
		self.assertEqual(result, "NOT_READY")
	def testReadyState1(self):
		result = self.oven.action('T')
		self.assertEqual(result, "Timer Set")
		result = self.oven.action('R')
		self.assertEqual(result, "Timer Reset")
		result = self.oven.action('T')
		self.assertEqual(result, "Timer Set")
		result = self.oven.action('R')
		self.assertEqual(result, "Timer Reset")
	def testCookingState1(self):
		result = self.oven.action('T')
		self.assertEqual(result, "Timer Set")
		result = self.oven.action('S')
		self.assertEqual(result, "Cooking")
		result = self.oven.action('H')
		self.assertEqual(result, "Cooking Paused")		
		result = self.oven.action('S')
		self.assertEqual(result, "Cooking")
		result = self.oven.action('H')
		self.assertEqual(result, "Cooking Paused")
	def testSafetyState1(self):
		result = self.oven.action('T')
		self.assertEqual(result, "Timer Set")
		result = self.oven.action('O')
		self.assertEqual(result, "Door Open")
		result = self.oven.action('C')
		self.assertEqual(result, "Door Closed")		
		result = self.oven.action('O')
		self.assertEqual(result, "Door Open")
		result = self.oven.action('C')
		self.assertEqual(result, "Door Closed")
		result = self.oven.action('R')
		self.assertEqual(result, "Timer Reset")
	def testSafetyState2(self):
		result = self.oven.action('T')
		self.assertEqual(result, "Timer Set")
		result = self.oven.action('S')
		self.assertEqual(result, "Cooking")
		result = self.oven.action('O')
		self.assertEqual(result, "Door opened - Cooking Stopped!")
		result = self.oven.action('C')
		self.assertEqual(result, "Door Closed")
		result = self.oven.action('R')
		self.assertEqual(result, "Timer Reset")
	def testCookingState2(self):
		result = self.oven.action('T')
		self.assertEqual(result, "Timer Set")
		result = self.oven.action('S')
		self.assertEqual(result, "Cooking")
		result = self.oven.action('E')
		self.assertEqual(result, "Timer Expired")
	def testBadInputNotReady(self):
		for inCommand in ('C', 'R', 'S', 'H', 'E'):
			result = self.oven.action(inCommand)
			self.assertEqual(result, "Operation not permitted")
	def testBadInputReady(self):
		self.oven.action('T')
		for inCommand in ('C', 'T', 'H', 'E'):
			result = self.oven.action(inCommand)
			self.assertEqual(result, "Operation not permitted")
	def testBadInputCooking(self):
		self.oven.action('T')
		self.oven.action('S')
		for inCommand in ('C', 'T', 'R', 'S'):
			result = self.oven.action(inCommand)
			self.assertEqual(result, "Operation not permitted")
	def testBadInputSafety(self):
		self.oven.action('T')
		self.oven.action('O')
		for inCommand in ('O', 'T', 'R', 'S', 'H', 'E'):
			result = self.oven.action(inCommand)
			self.assertEqual(result, "Operation not permitted")
if __name__ == '__main__':
    unittest.main()
