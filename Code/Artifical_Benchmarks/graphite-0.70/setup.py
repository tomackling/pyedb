from distutils.core import setup, Extension

setup(name = "graphite", version = "0.70",
	description = "Line and scatterplots of data, from a batch script.",
	author = "Greg Kochanski",
	url = "http://graphite.sourceforge.net",
	author_email = "gpk@kochanski.org",
	packages = ["graphite"],
	package_dir = {'graphite': '.'},
	scripts = ["bin/g_multiplot", "bin/g_widthplot"]
	)

