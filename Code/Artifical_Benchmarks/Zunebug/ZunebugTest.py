import sys, unittest, threading
import Zunebug, ZuneThread

class TestSeries(unittest.TestCase):

#	def test100(self):
#		testVal = Zunebug.zunebug(100)
#		self.assertEqual(testVal, 1980)
#	def test200(self):
#		testVal = Zunebug.zunebug(200)
#		self.assertEqual(testVal, 1980)
#	def test300(self):
#		testVal = Zunebug.zunebug(300)
#		self.assertEqual(testVal, 1980)
#	def test400(self):
#		testVal = Zunebug.zunebug(400)
#		self.assertEqual(testVal, 1981)
#	def test500(self):
#		testVal = Zunebug.zunebug(500)
#		self.assertEqual(testVal, 1981)
#	def test600(self):
#		testVal = Zunebug.zunebug(600)
#		self.assertEqual(testVal, 1981)
#	def test700(self):
#		testVal = Zunebug.zunebug(700)
#		self.assertEqual(testVal, 1981)
#	def test800(self):
#		testVal = Zunebug.zunebug(800)
#		self.assertEqual(testVal, 1982)
#	def test900(self):
#		testVal = Zunebug.zunebug(900)
#		self.assertEqual(testVal, 1982)
#	def test1000(self):
#		testVal = Zunebug.zunebug(1000)
#		self.assertEqual(testVal, 1982)
#	def test2000(self):
#		testVal = Zunebug.zunebug(2000)
#		self.assertEqual(testVal, 1985)
	def test3000(self):
		testVal = Zunebug.zunebug(3000)
		self.assertEqual(testVal, 1988)
	def test4000(self):
		testVal = Zunebug.zunebug(4000)
		self.assertEqual(testVal, 1990)
	def test5000(self):
		testVal = Zunebug.zunebug(5000)
		self.assertEqual(testVal, 1993)
	def testLEAP_FINAL_DAY_10593(self):
		zt = ZuneThread.zuneThread_10593()
		zt.daemon = True
		zt.start()
		zt.join(6.0)
		if zt.isAlive():
			self.assertEqual(1,0)
		else:
			testVal = Zunebug.zunebug(10593)
			self.assertEqual(testVal, 2008)
	def testLEAP_FINAL_DAY_366(self):
		zt = ZuneThread.zuneThread_366()
		zt.daemon = True
		zt.start()
		if zt.isAlive():
			self.assertEqual(1,0)
		else:
			testVal = Zunebug.zunebug(366)
			self.assertEqual(testVal, 1990)
	def testLEAP_FINAL_DAY_1827(self):
		zt = ZuneThread.zuneThread_1827()
		zt.daemon = True
		zt.start()
		zt.join(6.0)
		if zt.isAlive():
			self.assertEqual(1,0)
		else:
			testVal = Zunebug.zunebug(1827)
			self.assertEqual(testVal, 1984)
	def testLEAP_FINAL_DAY_3288(self):
		zt = ZuneThread.zuneThread_3288()
		zt.daemon = True
		zt.start()
		zt.join(6.0)
		if zt.isAlive():
			self.assertEqual(1,0)
		else:
			testVal = Zunebug.zunebug(3288)
			self.assertEqual(testVal, 1988)			
	def testLEAP_FINAL_DAY_4749(self):
		zt = ZuneThread.zuneThread_4749()
		zt.daemon = True
		zt.start()
		zt.join(6.0)
		if zt.isAlive():
			self.assertEqual(1,0)
		else:
			testVal = Zunebug.zunebug(4749)
			self.assertEqual(testVal, 1992)		
	def testLEAP_FINAL_DAY_6210(self):
		zt = ZuneThread.zuneThread_6210()
		zt.daemon = True
		zt.start()
		zt.join(6.0)
		if zt.isAlive():
			self.assertEqual(1,0)
		else:
			testVal = Zunebug.zunebug(6210)
			self.assertEqual(testVal, 1996)
			
if __name__ == '__main__':
    unittest.main()
