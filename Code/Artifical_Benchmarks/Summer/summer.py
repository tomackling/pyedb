
class summer(object):
	
	def __init__(self, offset):
		self.offset = offset

	def sumNums(self, n):
		sum = 0
		curNum = self.offset
		while curNum < n:
			sum += curNum
			curNum += self.offset
		return sum