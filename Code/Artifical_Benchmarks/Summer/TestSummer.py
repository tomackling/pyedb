import unittest
from summerpt2 import sum

class TestSeries(unittest.TestCase):
	
	def testOne(self):
		num = sum(1,10)
		self.assertEqual(num, 45)	
	def testOne1(self):
		num = sum(2,10)
		self.assertEqual(num, 20)
	def testOne2(self):
		num = sum(5,10)
		self.assertEqual(num, 5)

	def testOne4(self):
		num = sum(2,6)
		self.assertEqual(num, 6)
		
	def testOne3(self):
		num = sum(3,7)
		self.assertEqual(num, 3)
	def testSix(self):
		num = sum(3,10)
		self.assertEqual(num, 9)
		
	def testTwo(self):
		num = sum(10,30)
		self.assertEqual(num, 30)
	def testTwo1(self):
		num = sum(10,40)
		self.assertEqual(num, 60)
	def testTwo2(self):
		num = sum(10,50)
		self.assertEqual(num, 100)
	def testTwo3(self):
		num = sum(10,60)
		self.assertEqual(num, 150)
		
	def testFive(self):
		num = sum(15,45)
		self.assertEqual(num, 150)
		
	def testSeven(self):
		num = sum(11,45)
		self.assertEqual(num, 165)
	def testEight(self):
		num = sum(15,22)
		self.assertEqual(num, 45)
		
if __name__ == '__main__':
    unittest.main()