# Create the PreProcessor Object
TPP = TreePreProcessor()

# Get the total number of nodes in the source trees and the node count per line
numOfNodes = 0
nodesPerLine = dict()
for key in ASTList:
	numOfNodes += TPP.countNodes(ASTList[key])
	TPP.lineNodeCount(key,ASTList[key])

nodesPerLine = TPP.nodesPerLine
print 'Total number of nodes is',numOfNodes

# Get relative bug probabilities from tarantula
modulePath = 'Artifical_Benchmarks.Summer.'
testFile = 'TestSummer'
spiderNest = TarantulaClone()
table = spiderNest.generate_Line_Weight_Table(modulePath, filePath, testFile, sourceFiles)

# use data gathered to calculate weighting for each node on a particular line.
weightsTable = dict()
for key in table:
	if key in nodesPerLine:
		weightsTable[key] = (table[key]/nodesPerLine[key])*numOfNodes
		print key[0],key[1],'->',weightsTable[key]
	