import random

def mutate(genotype):
	prob = random.random()
	if prob > 0.5:
		insertionPoint = random.randint(0,len(genotype))
		insertionMaterial = bin(random.getrandbits(8)).lstrip('-0b')
		while len(insertionMaterial) < 8:
			insertionMaterial = '0'+insertionMaterial
		print insertionMaterial
		print ''
		result = genotype[:insertionPoint]+insertionMaterial+genotype[insertionPoint:]
	
	else:
		deletionLength = 8
		deletionPoint = random.randint(0,len(genotype)-deletionLength)
		result = genotype[:deletionPoint]+genotype[deletionPoint+deletionLength:]
	
	return result
	
genotype = bin(random.getrandbits(72)).lstrip('-0b')
while len(genotype) < 72:
	genotype = '0'+genotype

print genotype
print ''
print mutate(genotype)