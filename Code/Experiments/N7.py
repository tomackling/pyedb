import datetime
import os

class LogKeeper(object):

	def __init__(self, logName, dir):
		now = datetime.datetime.now()
		self.logFileTitle = logName+' '+str(now.day)+'-'+str(now.month)+'-'+str(now.year)+' '+str(now.hour)+str(now.minute)+str(now.second)+str(now.microsecond)
		self.dir = dir
		if not os.path.exists(dir):
			os.makedirs(dir)
		self.logStream = open(dir+self.logFileTitle+'.txt', 'w')
		
	def write_Log(self, strToWrite):
		self.logStream.write(strToWrite)
		
	def close_Log_Keeper(self):
		self.logStream.flush()
		self.logStream.close()
		
	def get_Stream(self):
		return self.logStream
		
MasterLog = LogKeeper('Master Log', os.getcwd()+'\\')
ExtendedLog = True