"""
Base class for Evolutionary Algorithm selectors
"""

class BaseSelector(object):

    def select(self, population):
        """
        Select an individual from the population.
        Returns the individual chosen.
        """
        pass

    def replace(self, population, offspring):
        """
        Replace some portion of the population with the offspring given.
        """
        pass
