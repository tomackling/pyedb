
import sys, os, random, math
sys.path.insert(0,os.getcwd())
import Code.Data_Writer.LogKeeper as LogKeeper
from Selector import BaseSelector

"""
Tournament select
"""

class Tournament(BaseSelector):

	tournamentSize = None # Tournament size

	def select(self, population):
		"""
		Select an individual from the population.
		Returns the individual chosen.
		"""
		tournament = []
		for i in range(0, self.tournamentSize):
			tournament.append(random.choice(population))
		indexBase = random.random()
		index = int(math.floor((indexBase**3)*(len(tournament)-1)))
		tournament = sorted(tournament, key=lambda ind: ind.getFitness()) # SORT MAY SLOW BE WARY - TESTING CONVERGE TIME
		return tournament[index]
		#return min(tournament, key=lambda ind : ind.getFitness())

	def selectTwo(self, population):
		ind1 = self.select(population)
		ind2 = self.select(population)
		return ind1, ind2
		
	def replace(self, population, offspring):
		"""
		Replace some portion of the population with the offspring given.
		"""
		numOffspringInPop = 0
		for individual in offspring:
			tournament = []
			for i in range(0, self.tournamentSize):
				tournament.append(random.choice(population))
			if max(tournament, key=lambda ind : ind.getFitness()).getFitness() >= individual.getFitness():
				population.remove(max(tournament, key=lambda ind : ind.getFitness()))
				population.append(individual)
				numOffspringInPop += 1
		#
		if LogKeeper.OffspringStats:
			LogKeeper.MasterLog.write_Log('Number of new individuals: '+str(numOffspringInPop)+'\n\n')
		##
