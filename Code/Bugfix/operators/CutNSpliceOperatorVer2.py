		
import random
from math import floor
from Operator import BaseOperator
from Code.Bugfix.Individual import Individual

class CutNSpliceOperatorVer2(BaseOperator):
	
	probability = None
	Xover = True
	
	def operate(self, selector, population):
		"""
		Uses the selector given to choose some individual(s) from the population given.
		
		Returns a new individual, produced from the parent individual(s) chosen.
		"""

		offspring0 = Individual('')
		offspring1 = Individual('')
		parents = (selector.selectTwo(population))
		parentGeno0 = parents[0].getGenotype()
		parentGeno1 = parents[1].getGenotype()
		result = self.cutNSplice(parentGeno0, parentGeno1)
		offspring0.setGenotype(result[0])
		offspring1.setGenotype(result[1])
		list = [offspring0,offspring1]
		return list
		
	def cutNSplice(self, genotype0, genotype1):
		
		offgeno0 = ''
		offgeno1 = ''

		absPos0 = int(floor(random.random()*len(genotype0)))
		absPos1 = int(floor(random.random()*len(genotype1)))
			
		offgeno0 = genotype0[:absPos0] + genotype1[absPos1:]
		offgeno1 = genotype1[:absPos1] + genotype0[absPos0:]

		list = [offgeno0, offgeno1]
		return list
