
import random
import math
from Operator import BaseOperator
from Code.Bugfix.Individual import Individual

class MutationOperator(BaseOperator):
	
	probability = None
	Xover = False

	def operate(self, selector, population):
		"""
		Uses the selector given to choose some individual(s) from the population given.
		
		Returns a new individual, produced from the parent individual(s) chosen.
		"""
		offspring = Individual('')
		offspring.setGenotype(self.mutate((selector.select(population)).getGenotype()))
		return offspring

	def mutate(self, genotype):
		prob = random.random()
		if prob > 0.5:
			insertionPoint = random.randint(0,len(genotype))
			insertionMaterial = bin(random.getrandbits(8)).lstrip('-0b')
			while len(insertionMaterial) < 8:
				insertionMaterial = '0'+insertionMaterial
			result = genotype[:insertionPoint]+insertionMaterial+genotype[insertionPoint:]
		else:
			deletionLength = 8
			if len(genotype) >= deletionLength:
				deletionPoint = random.randint(0,len(genotype)-deletionLength)
				result = genotype[:deletionPoint]+genotype[deletionPoint+deletionLength:]
			else:
				result = ''
		return result
