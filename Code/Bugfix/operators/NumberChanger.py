from Operator import BaseOperator

import copy, random, ast

"""
Performs a +/- 1 operation on Number nodes
"""

class NumberChanger(BaseOperator):
    
    def operate(self, selector, population):
        """
        Uses the selector given to choose an individual from the population given.
        
        Returns a new individual, produced from the parent individual.
        """
        offspring = copy.deepcopyz(selector.select(population))
        ASTNumberChanger().visit(offspring.myAst)
        return offspring
        
class ASTNumberChanger(ast.NodeTransformer):

    def visit_Num(self, node):
        return ast.copy_location(ast.Num(n = random.randint(0,4)),
                                 node)
