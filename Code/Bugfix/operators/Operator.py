import random

"""
Base class for Evolutionary Algorithm operators (like mutation, crossover).
"""

class BaseOperator(object):
    
    probability = None # Probability to use this operator

    def operate(self, selector, population):
        """
        Uses the selector given to choose some individual(s) from the population given.
        
        Returns a new individual, produced from the parent individual(s) chosen.
        """
        pass
