
from math import *
import random

offgeno0 = ''
offgeno1 = ''

genotype0 = '111011101110111011101110111011101110111011101110111011101110111011101110'
genotype1 = '000000000100000000000000000000000000000001000000000000000000000000000000'

incrIndex = [8,32,32]

numCutPts0 = floor((len(genotype0)/72)*3)
cutPos0 = random.randint(0, numCutPts0)
absPos0 = 0

count = 0
while count < cutPos0:
	absPos0 += incrIndex[count%3]
	count += 1

numCutPts1 = floor((len(genotype1)/72)*3)
cutPos1 = random.randint(0, numCutPts1)
absPos1 = 0

count = 0
while count < cutPos1:
	absPos1 += incrIndex[count%3]
	count += 1
	
offgeno0 = genotype0[:absPos0] + genotype1[absPos1:]
offgeno1 = genotype1[:absPos1] + genotype0[absPos0:]

list = [offgeno0, offgeno1]
print list