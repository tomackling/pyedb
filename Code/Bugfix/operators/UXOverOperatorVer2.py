
import random
from Operator import BaseOperator
from Code.Bugfix.Individual import Individual

class UXOverOperatorVer2(BaseOperator):
	
	probability = None
	Xover = True
	
	def operate(self, selector, population):
		"""
		Uses the selector given to choose some individual(s) from the population given.
		
		Returns a new individual, produced from the parent individual(s) chosen.
		"""
		offspring0 = Individual('')
		offspring1 = Individual('')
		parents = (selector.selectTwo(population))
		parentGeno0 = parents[0].getGenotype()
		parentGeno1 = parents[1].getGenotype()
		result = self.UXover(parentGeno0, parentGeno1)
		offspring0.setGenotype(result[0])
		offspring1.setGenotype(result[1])
		list = [offspring0,offspring1]
		return list
		
		
	def UXover(self, genotype0, genotype1):
	
		while len(genotype0) < 96:
			genotype0 += str(random.randint(0,1))
		while len(genotype1) < 96:
			genotype1 += str(random.randint(0,1))
			
		minLength = min(len(genotype0), len(genotype1))
		
		offgeno0 = ''
		offgeno1 = ''
		
		for iter in range(0,len(genotype0)-1):
			if random.random() <= 0.5:
				offgeno0 = offgeno0 + genotype1[iter]
				offgeno1 = offgeno1 + genotype0[iter]
			else:
				offgeno0 = offgeno0 + genotype0[iter]
				offgeno1 = offgeno1 + genotype1[iter]
		
		if len(genotype0) == minLength:
			offgeno1 = offgeno1 + genotype1[minLength:]
		else:
			offgeno0 = offgeno0 + genotype0[minLength:]
		
		list = [offgeno0, offgeno1]
		return list
		
