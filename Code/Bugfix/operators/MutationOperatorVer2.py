
import random
import math
from Operator import BaseOperator
from Code.Bugfix.Individual import Individual

class MutationOperatorVer2(BaseOperator):
	
	probability = None
	Xover = False

	def operate(self, selector, population):
		"""
		Uses the selector given to choose some individual(s) from the population given.
		
		Returns a new individual, produced from the parent individual(s) chosen.
		"""
		offspring = Individual('')
		offspring.setGenotype(self.mutate((selector.select(population)).getGenotype()))
		return offspring

	def mutate(self, genotype):

		replacementPoint = random.randint(0,len(genotype))
		if replacementPoint >= len(genotype)-8:
			replacementPoint = len(genotype)-8
		replacementMaterial = bin(random.getrandbits(8)).lstrip('-0b')
		while len(replacementMaterial) < 8:
			replacementMaterial = '0'+replacementMaterial
		result = genotype[:replacementPoint]+replacementMaterial+genotype[replacementPoint+8:]

		return result
