#cutNSplice Test

import random
from CutNSpliceOperator import CutNSpliceOperator

op = CutNSpliceOperator()

a1 = '111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111'
a2 = '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'
stringList = []

for x in xrange(0,50):
	stringList.append(a1)
	stringList.append(a2)
	
sumLength = 0
for st in stringList:
	sumLength += len(st)
	
initAvgLength = sumLength/len(stringList)
averageLength = initAvgLength

while averageLength == initAvgLength:

	indA = random.randint(0,99)
	indB = random.randint(0,99)

	result = op.cutNSplice(stringList[indA],stringList[indB])

	stringList[indA] = result[0]
	stringList[indB] = result[1]
	
	sumLength = 0
	for st in stringList:
		sumLength += len(st)
		print st
	print
	print 'gap'
	print 
	
	averageLength = sumLength/len(stringList)