
import sys, os, copy, random, time, threading
sys.path.insert(0,os.getcwd())
import Code.Data_Writer.LogKeeper as LogKeeper
from Code.PatcherUtilities.unparse import Unparser
import AspectFlags

"""
Breeder.

Breeds a population of individuals over a number of generations.
"""

class Breeder(object):
	
	population = []       # List of individuals that forms the population
	operators = []        # List of operators for use in the breeder
	selector = None       # Selector to use 
	evaluator = None      # Evaluator to use
	populationSize = None # Size of the population
	maxGenerations = None # Maximum number of generations to run for

	def breed(self):
		"""
		Breeds the population over a number of generations.
		If an individual reaches fitness of zero, terminates.
		If the number of generations reaches maxGenerations (if supplied), terminates
		"""
		print('evaluating initial pop')
		#fits=[]
		for individual in self.population:
			self.evaluator.evaluate(individual, 0)
			ind = individual
		#	opnum = int(ind.getGenotype()[0:2],2)
		#	locnum = int(ind.getGenotype()[2:17],2)
		#	changenum = int(ind.getGenotype()[17:32],2)
		#	print(str(locnum) + ' ' + str(opnum) + ' ' + str(changenum) +
		#	      ' ' + str(ind.getFitness()))
		#	fits.append(ind.getFitness())
	        
		#print('len fits: '+ str(len(fits)) + str(len(fits)/8) + str(len(fits)%8))
		#for i in range(len(fits)/32):
		#	outstring=''
		#	for j in range(32):
		#		outstring += ' ' + str(fits[i*8+j])
		#	print(outstring)
		

		print('now the alg')
		for generationNumber in xrange(0, self.maxGenerations+1):
			if LogKeeper.individuals:
				#
				maxGenoLength = (-sys.maxint-1)
				minGenoLength = sys.maxint
				averageGenoLength = 0
				for ind in self.population:
					if LogKeeper.printIndividuals:
						LogKeeper.MasterLog.write_Log(ind.getGenotype()+'\n')	
					LogKeeper.MasterLog.write_Log('fitness: '+str(ind.getFitness())+'\n')
					opnum = int(ind.getGenotype()[0:2],2)
					locnum = int(ind.getGenotype()[2:17],2)
					changenum = int(ind.getGenotype()[17:32],2)
                                        print(str(locnum) + ' ' + str(opnum) + ' ' + str(changenum) +
					      ' ' + str(ind.getFitness()))
				
					length = len(ind.getGenotype())
					if length > maxGenoLength:
						maxGenoLength = length
					if length < minGenoLength:
						minGenoLength = length
					averageGenoLength += length
				averageGenoLength = float(averageGenoLength)/float(self.populationSize)
				LogKeeper.MasterLog.write_Log("generation:%d maxGenomeLength:%d minGenomeLength:%d averageGenomeLength:%f\n" % (generationNumber, maxGenoLength, minGenoLength, averageGenoLength))
				##
			#
			best = min(self.population, key=lambda ind: ind.getFitness())
			worst = max(self.population, key=lambda ind: ind.getFitness())
			total = 0
			for ind in self.population:
				total += ind.getFitness()
			average = total/float(len(self.population))
			LogKeeper.MasterLog.write_Log("generation:%d bestFitness:%f worstFitness:%f averageFitness:%f\n" % (generationNumber, best.getFitness(), worst.getFitness(), average))
			(LogKeeper.MasterLog.get_Stream()).flush()
			print "generation:%d bestFitness:%f worstFitness:%f averageFitness:%f\n" % (generationNumber, best.getFitness(), worst.getFitness(), average)
			##
			
			if (best.getFitness() <= 0) and (not AspectFlags.contAfterSol):
				for source in best.getASTList():
					Unparser(best.getASTList()[source], LogKeeper.MasterLog.get_Stream())
				break
			if generationNumber == self.maxGenerations:
				break
			
			offspring = []
			for individual in self.population:
				probability = random.random()
				for operator in self.operators:
					if probability < operator.probability:
						if not operator.Xover:
							offspring.append(operator.operate(self.selector, self.population))
						else:
							for child in operator.operate(self.selector, self.population):
								offspring.append(child)
								
			#
			if LogKeeper.OffspringStats:
				LogKeeper.MasterLog.write_Log('Number of offspring generted: '+str(len(offspring))+'\n')
			##

			# Might be able to use a map here instead for performance?
			# But this might break things...
			opLength = 32
			maxLength = AspectFlags.maxNumChanges*opLength
			for individual in offspring:
				if len(individual.getGenotype()) < opLength:
					while len(individual.getGenotype()) < opLength:
						if random.random() < 0.5 :
							individual.setGenotype(individual.getGenotype()+'0')
						else:
							individual.setGenotype(individual.getGenotype()+'1')
				if len(individual.getGenotype()) > maxLength:
					individual.setGenotype(individual.getGenotype()[0:maxLength])
				self.evaluator.evaluate(individual, generationNumber)
				if individual.getFitness() == 0:
					individual.setFitness(0 - ((maxLength/float(len(individual.getGenotype()))) - 1))
																
			self.selector.replace(self.population, offspring)
