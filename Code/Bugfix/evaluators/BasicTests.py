from Evaluator import BaseEvaluator

import sys, types, unittest

"""
Basic test runner
"""

class BasicTests(BaseEvaluator):

    def __init__(self, testFile):
        self.testFile = testFile
        self.logFile = open("testRunner.log", 'w')

    def evaluate(self, individual):
        """
        Runs the test on the individual, sets individual.fitness based on the number of failures
        """

        newModule = types.ModuleType('microwave', 'compiled from ' + str(individual)) # TODO make this more dynamic
        testModule = types.ModuleType('testSuite', 'compiled tests')

        code = compile(individual.myAst, "compiled microwave", 'exec')
        exec code in newModule.__dict__

        sys.modules['microwave'] = newModule
        
        code = compile(open(self.testFile).read(), self.testFile, 'exec')
        exec code in testModule.__dict__

        loader = unittest.TestLoader()
        suite = loader.loadTestsFromModule(testModule)
        runner = unittest.TextTestRunner(stream=self.logFile)
        result = runner.run(suite)
        individual.fitness = len(result.failures) + len(result.errors)
