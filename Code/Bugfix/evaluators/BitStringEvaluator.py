
import sys, os, types, unittest
sys.path.insert(0,os.getcwd()) 
import AspectFlags as AspectFlags
import Code.Data_Writer.LogKeeper as LogKeeper
from Code.PatcherUtilities.TestProcesses import testProcess
from Code.PatcherUtilities.unparse import Unparser
from Code.Bugfix.evaluators.Evaluator import BaseEvaluator
from Code.Bugfix.Individual import Individual
from multiprocessing import Process
from multiprocessing import Queue
import Queue as QUE

class BitStringEvaluator(BaseEvaluator):
	
	def __init__(self, patcher, testFilePath, testFile):
		self.patcher = patcher
		self.testFilePath = testFilePath
		self.testFile = testFile
	
	def evaluate(self, individual, generation):
		
		#generate patch
		self.patcher.set_Up_Executor()
		self.patcher.patch_With_Genotype(individual.getGenotype())
		#for source in self.patcher.get_ASTList_For_Evaluation():
			#Unparser(self.patcher.get_ASTList_For_Evaluation()[source])
		individual.setASTList(self.patcher.get_ASTList_For_Evaluation())
		
		# Now evaluate individual
		
		q = Queue(1)
		
		args = dict()
		args['testFilePath'] = self.testFilePath
		args['testFile'] = self.testFile
		args['ind'] = individual
		
		q.put(args)
		
		tester = Process(target=testProcess,args=(q,))
		tester.daemon = True		
		tester.start()
		tester.join(AspectFlags.timeout*4)
		try:
			fitness = q.get_nowait()
			if type(fitness).__name__ != 'dict':
				individual.setFitness(fitness)
				#
				if LogKeeper.fitnessValue:
					LogKeeper.MasterLog.write_Log('fitness of patch is: '+str(individual.fitness)+'\n')
				##
			elif tester.is_alive(): # max(worst) fitness = ttl number of tests, should be loaded from AspectFlags
				individual.setFitness(AspectFlags.numOfTests + 1)
				#
				if LogKeeper.fitnessValue:
					LogKeeper.MasterLog.write_Log('fitness of patch is: '+str(individual.fitness)+' infinate loop\n')
				##
				tester.terminate()
			else:
				individual.setFitness(AspectFlags.numOfTests + 1)
				#
				if LogKeeper.fitnessValue:
					LogKeeper.MasterLog.write_Log('fitness of patch is: '+str(individual.fitness)+' failed to run\n')
				##
		except QUE.Empty:
			if tester.is_alive(): # max(worst) fitness = ttl number of tests, should be loaded from AspectFlags
				individual.setFitness(AspectFlags.numOfTests + 1)
				#
				if LogKeeper.fitnessValue:
					LogKeeper.MasterLog.write_Log('fitness of patch is: '+str(individual.fitness)+' infinate loop\n')
				##
				tester.terminate()
			else:
				individual.setFitness(AspectFlags.numOfTests + 1)
				#
				if LogKeeper.fitnessValue:
					LogKeeper.MasterLog.write_Log('fitness of patch is: '+str(individual.fitness)+' failed to run\n')
				##
				
