"""
Base class for Evolutionary Algorithm evaluators
"""

class BaseEvaluator(object):
    
    def evaluate(self, individual):
        """
        Evaluate an individual, and sets its individual.fitness parameter.
        """
        pass
