
# BitStringEvaluator Test

from evaluators.BitStringEvaluator import *
from Individual import Individual
from PatcherUtilities.PreProcessor import PreProcessor
from PatcherUtilities.Patcher import Patcher

modulePath = 'PatcherUtilities.Artifical_Benchmarks.Summer.'
filePath = 'C:\\Users\\Thomas\\Summer Research\\code\\Artifical_Benchmarks\\Summer\\'
sourceFiles = []
sourceFiles.append('summer')
sourceFiles.append('summerpt2')
testFile = 'TestSummer'

PP = PreProcessor(modulePath, filePath, sourceFiles, testFile)
PP.preProcess()

result = PP.getResults()	# Result is tuple of form: ASTList, numberOfNodes, treeWeights
ASTList = result[0]
numberOfNodes = result[1]
treeWeights = result[2]

patchApplier = Patcher(ASTList, numberOfNodes, treeWeights)

testIndividual = Individual('00000001' + '00000000000000000000000000100011' + '00000000000000000000000000000001'+'00000001' + '00000000000000000000000001010000' + '00000000000000000000000000000001')

evaluator = BitStringEvaluator(patchApplier, filePath, testFile)

evaluator.evaluate(testIndividual, 0)