from Breeder import Breeder
from Individual import Individual
from selectors.Tournament import Tournament
from operators.NumberChanger import NumberChanger
from evaluators.BasicTests import BasicTests

class CLI():

    def main(self):
        # Later, we will use optparse to bring in the breeders arguments dynamically
        # For now, lets just set them here.
        breeder = Breeder()
        breeder.selector = Tournament()
        breeder.selector.tournamentSize = 5

        breeder.evaluator = BasicTests("TestMicrowave.py")

        numOp = NumberChanger()
        numOp.probability = 1.0
        breeder.operators = [numOp]

        breeder.maxGenerations = 100
        breeder.populationSize = 500

        breeder.baseIndividual = Individual("microwave.py")

        breeder.breed()
