
import ast

"""
Individual - a patch to apply to programs
"""

class Individual(object):
	
	#fitness = None # Fitness of this individual
	#genotype = None # Genotype: Bitstring of instructions processed when patching the original program.
	#myASTList = None # Patched ASTs for this individual
	
	def __init__(self, genotype):
		self.genotype = genotype
		self.myASTList = None
		self.fitness = None
		
	def setASTList(self,ASTList):
		self.myASTList = ASTList

	def setFitness(self,fitness):
		self.fitness = fitness

	def setGenotype(self,geno):
		self.genotype = geno

	def getASTList(self):
		return self.myASTList
	def getFitness(self):
		return self.fitness
	def getGenotype(self):
		return self.genotype
