if __name__ == '__main__':
	import sys, os, types,copy
	sys.path.insert(0,'C:\\Users\\Thomas\\Desktop\\Patch Evolution New\\')
	from ast import *
	from Code.PatcherUtilities.TreePreProcessor import TreePreProcessor
	from Code.PatcherUtilities.PreProcessor import PreProcessor
	from Code.PatcherUtilities.OpExecutor.OpClasses import swap_Operator_NT
	from Code.PatcherUtilities.Patcher import Patcher
	from Code.pyflakes.checker import Checker

	modulePath = 'Code.Artifical_Benchmarks.smallworld.'
	filePath = 'C:\\Users\\Thomas\\Desktop\\Patch Evolution New\\Code\\Artifical_Benchmarks\\smallworld\\'
	sourceFiles = ['smallworld']
	testFile = 'testsmallworld'

	ASTList = dict()
	for source in sourceFiles:
		myAst = compile(open(filePath+source+'.py').read(), filePath+source+'.py', 'exec', PyCF_ONLY_AST)
		ASTList[source] = myAst

	PP = PreProcessor(modulePath, filePath, sourceFiles, testFile)
	PP.preProcess()
	
	res = PP.getResults()
	ASTList = res[0]
	opList = res[1]
	print opList
	varList = res[2]
	print varList
	
	swapper = swap_Operator_NT(ASTList, 3, 'Gt', 'LtE', 29, 'smallworld')
	swapper.traverse_and_transform('BLAH!')
	
	pat = Patcher(ASTList, opList, varList)
	pat.set_Up_Executor()
	pat.patch_With_Genotype('00000000011111010000000000000000')
	
	#TPP = TreePreProcessor()
	#TPP.lineNodeCount('smallworld',ASTList['smallworld'])
	#TPP.visitAll(ASTList['smallworld'])
	
