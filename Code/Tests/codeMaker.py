
if __name__ == '__main__':
	import sys, os, types
	sys.path.insert(0,os.getcwd())

	operations = '0000110010101100100010000111100100111010100111001101111110000001001011010011001010100001000001011001100011011110000001001000011110101101000100011000111000100000100011001100010110110010000101011101101100111010000011001010110010001000011110011001101011010101010100001100001011101011110011110101001100100101110010000110011000001100101011001000100001111001100110101101010101010000110001100000110010101100100010000111100110011010110101010101000011000110001010011000110010000101'

	from Code.pyflakes.checker import Checker
	from Code.PatcherUtilities.unparse import Unparser
	from Code.PatcherUtilities.PreProcessor import PreProcessor
	from Code.PatcherUtilities.Patcher import Patcher
	from Code.PatcherUtilities.OpExecutor.OpExecutor import *
	from ast import *

	modulePath = 'Code.Artifical_Benchmarks.Middle.'
	filePath = 'C:\\Users\\Thomas\\Desktop\\Patch Evolution New\\Code\\Artifical_Benchmarks\\Middle\\'
	sourceFiles = ['middle']
	testFile = 'MiddleTest'

	ASTList = dict()
	for source in sourceFiles:
		myAst = compile(open(filePath+source+'.py').read(), filePath+source+'.py', 'exec', PyCF_ONLY_AST)
		ASTList[source] = myAst

	PP = PreProcessor(modulePath, filePath, sourceFiles, testFile)
	PP.preProcess()

	result = PP.getResults()	# Result is tuple of form: ASTList, numberOfNodes, treeWeights
	ASTList = result[0]
	numberOfNodes = result[1]
	treeWeights = result[2]
	varLists = result[3]

	patchApplier = Patcher(ASTList	, numberOfNodes, treeWeights, varLists)
	patchApplier.set_Up_Executor()
	patchApplier.patch_With_Genotype(operations)
	NewASTs = patchApplier.get_ASTList_For_Evaluation()

	newModule = types.ModuleType('middle', 'compiled')
	code = compile(ASTList['middle'], "compiled module", 'exec')
	try:
		exec code in newModule.__dict__
		sys.modules['middle'] = newModule
	except SyntaxError:
		print 'compilation failure'
	
	import middle
	print middle.middleFunc(2,1,3)
	
	Unparser(NewASTs, file=open('newMiddle.py','w'))