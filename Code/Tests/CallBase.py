def callbase(obj, base, methodname='__init__', args=(), kw={}, raiseIfMissing=None):
	try:
		method = getattr(base, methodname)
	except AttributeError:
		if raiseIfMissing:
			raise CallbaseError, methodname
		return None
	if args is None: args = ()
	return method(obj, *args, **kw)