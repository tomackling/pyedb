
if __name__ == '__main__':
	import sys, os, types
	sys.path.insert(0,'./')

	from Code.pyflakes.checker import Checker
	from Code.PatcherUtilities.TreePreProcessor import TreePreProcessor
	from Code.PatcherUtilities.OpExecutor.OpClasses import swap_Var_Name_NT
	from Code.PatcherUtilities.PreProcessor import PreProcessor
	from Code.PatcherUtilities.Patcher import Patcher
	from Code.PatcherUtilities.unparse import Unparser
	from Code.Bugfix.evaluators.BitStringEvaluator import BitStringEvaluator
	from Code.Bugfix.Individual import Individual
	from ast import *
	import Code.Data_Writer.LogKeeper as LogKeeper
	import AspectFlags

	LogKeeper.MasterLog = LogKeeper.LogKeeper('tester','')
	
	modulePath = 'Code.Artifical_Benchmarks.smallworld.'
	filePath = './Code/Artifical_Benchmarks/smallworld/'
	testFilePath = './Code/Artifical_Benchmarks/smallworld/'
	sourceFiles = ['smallworld']
	testFile = 'testsmallworld'

	ASTList = dict()
	for source in sourceFiles:
		myAst = compile(open(filePath+source+'.py').read(), filePath+source+'.py', 'exec', PyCF_ONLY_AST)
		ASTList[source] = myAst

	PP = PreProcessor(modulePath, filePath, sourceFiles, testFile)
	PP.preProcess()

	result = PP.getResults()	# Result is tuple of form: ASTList, numberOfNodes, treeWeights
	ASTList = result[0]
	opLists = result[1]
	varLists = result[2]

	#TPP = TreePreProcessor()
	#TPP.visitAll(ASTList['middle'])

	AspectFlags.varWeights = False
	AspectFlags.opWeights = False
	
	#pat = Patcher(ASTList, opLists, varLists)
	#pat.set_Up_Executor()
	#pat.patch_With_Genotype('11010011001001101100011010100110')
	#ASTList = pat.get_ASTList_For_Evaluation()
	#for source in ASTList:
		#Unparser(ASTList[source])
		#print 

	
	
	pat = Patcher(ASTList, opLists, varLists)
	eval = BitStringEvaluator(pat,filePath,testFile)
	
	ind  = Individual(sys.argv[1])
	eval.evaluate(ind, 0)
	print ind.getFitness()
	
	ASTList = ind.getASTList()

#	TPP = TreePreProcessor()
	#for source in ASTList:
		#TPP.visitAll(ASTList[source])		

	for source in ASTList:
		Unparser(ASTList[source])
