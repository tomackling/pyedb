
#
# Testing script for checking the frequency of nodes with high weighting having visits aplied to them,
# by randomly generated offset, and extents. In the standard operation.
#

import random
from sys import *
from ast import *
from OpExecutor.OpExecutor import *
from TreePreProcessor import *
from TarantulaClone import *

# Get sourcefiles and compile into asts
ASTList = dict()
filePath = 'C:\\Users\\Thomas\\Summer Research\\code\\Artifical_Benchmarks\\Summer\\'
sourceFiles = []
sourceFiles.append('summer')
sourceFiles.append('summerpt2')
for source in sourceFiles:
	myAst = compile(open(filePath+source+'.py').read(), filePath+source+'.py', 'exec', PyCF_ONLY_AST)
	ASTList[source] = myAst

# Create the PreProcessor Object
TPP = TreePreProcessor()

# Get the total number of nodes in the source trees and the node count per line
numOfNodes = 0
nodesPerLine = dict()
for key in ASTList:
	numOfNodes += TPP.countNodes(ASTList[key])
	TPP.lineNodeCount(key,ASTList[key])

nodesPerLine = TPP.nodesPerLine
#print 'Total number of nodes is',numOfNodes

# Get relative bug probabilities from tarantula
modulePath = 'Artifical_Benchmarks.Summer.'
testFile = 'TestSummer'
spiderNest = TarantulaClone()
table = spiderNest.generate_Line_Weight_Table(modulePath, filePath, testFile, sourceFiles)

# use data gathered to calculate weighting for each node on a particular line.
weightsTable = dict()
for key in table:
	if key in nodesPerLine:
		weightsTable[key] = (table[key]/nodesPerLine[key])*numOfNodes
		#print key[0],key[1],'->',weightsTable[key]

exe = OpExecutor(ASTList, numOfNodes, weightsTable)

op = '00000001'
for i in range(5000):
	offset = bin(random.getrandbits(32)).lstrip('-0b')
	extent = bin(random.getrandbits(32)).lstrip('-0b')
	while len(offset) < 32:
		offset = '0' + offset
	while len(extent) < 32:
		extent = '0' + extent
	operation = op+offset+extent
	exe.executeOp(operation)


