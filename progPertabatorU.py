# Randomly Generated Erroneous Programs

if __name__ == '__main__':
	import sys, os, types, random, ConfigParser
	sys.path.insert(0,os.getcwd())

	from Code.pyflakes.checker import Checker
	from Code.PatcherUtilities.TreePreProcessor import TreePreProcessor
	from Code.PatcherUtilities.OpExecutor.OpClasses import swap_Var_Name_NT
	from Code.PatcherUtilities.PreProcessor import PreProcessor
	from Code.PatcherUtilities.Patcher import Patcher
	from Code.PatcherUtilities.unparse import Unparser
	from Code.Bugfix.evaluators.BitStringEvaluator import BitStringEvaluator
	from Code.Bugfix.Individual import Individual
	from ast import *
	import AspectFlags

	configPaths = ConfigParser.SafeConfigParser()
	configPaths.read(sys.argv[1])
	
	modulePath = configPaths.get('Paths','modulePath')
	filePath = os.getcwd()+configPaths.get('Paths', 'filePath')
	sourceFiles = []
	for srcFileNum, srcFile in configPaths.items('SourceFiles'):
		sourceFiles.append(srcFile)
	testFile = configPaths.get('TestFiles', 'testFile1')

	ASTList = dict()
	for source in sourceFiles:
		myAst = compile(open(filePath+source+'.py').read(), filePath+source+'.py', 'exec', PyCF_ONLY_AST)
		ASTList[source] = myAst

	PP = PreProcessor(modulePath, filePath, sourceFiles, testFile)
	PP.preProcess()

	result = PP.getResults()
	ASTList = result[0]
	opLists = result[1]
	varLists = result[2]

	AspectFlags.varWeights = False
	AspectFlags.opWeights = False
	
	configList = []
	set = (sys.argv[1].strip('.cfg'))+'Pertabations'
	if os.path.exists(os.getcwd()+'/'+set) == False:
		os.mkdir(os.getcwd()+'/'+set)

	pat = Patcher(ASTList, opLists, varLists)
	eval = BitStringEvaluator(pat,filePath,testFile)
	
	for x in xrange(1,6):
		progCount = 0
		while progCount < 6:
			geno = bin(random.getrandbits(32*x)).lstrip('-0b')
			while len(geno) < 32*x:
				geno = '0'+geno
			ind = Individual(geno)
			eval.evaluate(ind, 0)
			if ind.fitness > 0:
				ASTList = ind.myASTList
				newSource = str(x)+'-'+str(progCount)
				newModulePath = set+'.'+newSource+'.'
				sourceCount = 1
				for source in ASTList:
					if os.path.exists(os.getcwd()+'/'+set+'/'+newSource) == False:
						 os.mkdir(os.getcwd()+'/'+set+'/'+newSource)
					Unparser(ASTList[source], open(os.getcwd()+'/'+set+'/'+newSource+'/'+source+'.py', 'w'))
					configPaths.set('SourceFiles', 'sourceFile'+str(sourceCount),source)
					sourceCount += 1
	       			configPaths.set('Paths','modulePath',newModulePath)
	       			configPaths.set('Paths', 'filePath','/'+set+'/'+newSource+'/')
	       			configPaths.set('TestFiles', 'testFile1',testFile)
	       			configList.append(os.getcwd()+'/'+set+'/'+set+'-'+str(x)+'-'+str(progCount)+'.cfg')
	       			configPaths.write(open(os.getcwd()+'/'+set+'/'+set+'-'+str(x)+'-'+str(progCount)+'.cfg', 'w'))
	       			progCount += 1
	
	configListFile = open('ConfigList'+set, 'w')
	for item in configList:
		configListFile.write(item+'\n')

		
