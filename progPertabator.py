# Randomly Generated Erroneous Programs

if __name__ == '__main__':
	import sys, os, types, random, ConfigParser
	sys.path.insert(0,os.getcwd())

	from Code.pyflakes.checker import Checker
	from Code.PatcherUtilities.TreePreProcessor import TreePreProcessor
	from Code.PatcherUtilities.OpExecutor.OpClasses import swap_Var_Name_NT
	from Code.PatcherUtilities.PreProcessor import PreProcessor
	from Code.PatcherUtilities.Patcher import Patcher
	from Code.PatcherUtilities.unparse import Unparser
	from ast import *
	import AspectFlags

	configPaths = ConfigParser.SafeConfigParser()
	configPaths.read(sys.argv[1])
	
	modulePath = configPaths.get('Paths','modulePath')
	filePath = os.getcwd()+configPaths.get('Paths', 'filePath')
	sourceFiles = []
	for srcFileNum, srcFile in configPaths.items('SourceFiles'):
		sourceFiles.append(srcFile)
	testFile = configPaths.get('TestFiles', 'testFile1')

	ASTList = dict()
	for source in sourceFiles:
		myAst = compile(open(filePath+source+'.py').read(), filePath+source+'.py', 'exec', PyCF_ONLY_AST)
		ASTList[source] = myAst

	PP = PreProcessor(modulePath, filePath, sourceFiles, testFile)
	PP.preProcess()

	result = PP.getResults()
	ASTList = result[0]
	opLists = result[1]
	varLists = result[2]

	AspectFlags.varWeights = False
	AspectFlags.opWeights = False
	
	configList = []
	set = (sys.argv[1].strip('.cfg'))+'Pertabations'
	if os.path.exists(os.getcwd()+'\\'+set) == False:
		os.mkdir(os.getcwd()+'\\'+set)
	newPath = os.getcwd()+'\\'+set+'\\'
	
	for x in xrange(1,6):
		progCount = 0
		while progCount < 15:
			geno = bin(random.getrandbits(32*x)).lstrip('-0b')
			while len(geno) < 32*x:
				geno = '0'+geno
			pat = Patcher(ASTList, opLists, varLists)
			pat.set_Up_Executor()
			pat.patch_With_Genotype(geno)
			ASTList = pat.get_ASTList_For_Evaluation()
			newModulePath = set+'.'
			sourceCount = 1
			for source in ASTList:
				newSource = source+'-'+str(x)+'-'+str(progCount)
				Unparser(ASTList[source], open(newPath+newSource+'.py', 'w'))
				configPaths.set('SourceFiles', 'sourceFile'+str(sourceCount),newSource)
				sourceCount += 1
			configPaths.set('Paths','modulePath',newModulePath)
			configPaths.set('Paths', 'filePath',newPath)
			configPaths.set('TestFiles', 'testFile1',testFile)
			configList.append(newPath+set+'-'+str(x)+'-'+str(progCount)+'.cfg')
			configPaths.write(open(newPath+set+'-'+str(x)+'-'+str(progCount)+'.cfg', 'w'))
			progCount += 1
	
	configListFile = open(newPath+set+'ConfigList', 'w')
	for item in configList:
		configListFile.write(item+'\n')

		