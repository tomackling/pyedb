# pyEDB - **py**thon **E**volutionary **D**e**B**ugging framework #

This is an opensource repository of the pyEDB experimental framework.

pyEDB is a tool for automated software repair of python programs.

As per: http://dl.acm.org/citation.cfm?id=2001768

It uses genetic programming in order to provide fixes for programs in the form of patches.

Patches are the subject of evolution in this framework and compete in a survival of the fittest content where fitness is a function of tests passed.